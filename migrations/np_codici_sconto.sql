-- --------------------------------------------------------

--
-- Struttura della tabella `np_codici_sconto`
--

CREATE TABLE IF NOT EXISTS `np_codici_sconto` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `codice` varchar(100) NOT NULL,
  `link` text NULL,
  `logo` text NOT NULL,
  `testo` text NULL,
  `d-create` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
