
<!doctype html >
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="it-IT" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->
<head>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="https://www.prestitotto.it/xmlrpc.php" />
    <link rel="icon" type="image/png" href="https://www.prestitotto.it/wp-content/uploads/2020/03/faviconprestitotto_new.png">
<!-- This site is optimized with the Yoast SEO Premium plugin v8.4 - https://yoast.com/wordpress/plugins/seo/ -->
<title>Codici sconto - Prestitotto</title>
<meta name="description" content="Codici sconto - Prestitotto."/>
<link rel="canonical" href="https://www.prestitotto.it/codici-sconto/" />
<meta property="og:locale" content="it_IT" />
<meta property="og:type" content="object" />
<meta property="og:title" content="Codici sconto - Prestitotto" />
<meta property="og:description" content="Codici sconto - Prestitotto." />
<meta property="og:url" content="https://www.prestitotto.it/codici-sconto/" />
<meta property="og:site_name" content="Prestitotto" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="Codici sconto - Prestitotto." />
<meta name="twitter:title" content="Codici sconto - Prestitotto" />
<script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"Organization","url":"https:\/\/www.prestitotto.it\/","sameAs":[],"@id":"https:\/\/www.prestitotto.it\/#organization","name":"Prestitotto Srl","logo":"https:\/\/www.prestitotto.it\/wp-content\/uploads\/2020\/03\/logo_prestitotto.jpg"}</script>
<!-- / Yoast SEO Premium plugin. -->

<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<script type="text/javascript">
	window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.prestitotto.it\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.14"}};
	!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='popupaoc-public-style-css'  href='https://www.prestitotto.it/wp-content/plugins/popup-anything-on-click/assets/css/popupaoc-public-style.css?ver=1.7.4' type='text/css' media='all' />
<link rel='stylesheet' id='strumenti-css-css'  href='https://www.prestitotto.it/wp-content/plugins/strumenti_calcolo/assets/css/strumenti_calcolo.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='cookie-consent-style-css'  href='https://www.prestitotto.it/wp-content/plugins/uk-cookie-consent/assets/css/style.css?ver=4.9.14' type='text/css' media='all' />
<link rel='stylesheet' id='tooltipy-default-style-css'  href='https://www.prestitotto.it/wp-content/plugins/wp-tooltipy/assets/style.css?ver=4.9.14' type='text/css' media='all' />
<link rel='stylesheet' id='mediaelement-css'  href='https://www.prestitotto.it/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css?ver=4.2.6-78496d1' type='text/css' media='all' />
<link rel='stylesheet' id='wp-mediaelement-css'  href='https://www.prestitotto.it/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=4.9.14' type='text/css' media='all' />
<link rel='stylesheet' id='td-plugin-multi-purpose-css'  href='https://www.prestitotto.it/wp-content/plugins/td-composer/td-multi-purpose/style.css?ver=93bc67e89bc4998b79b2f1fa650def01' type='text/css' media='all' />
<link rel='stylesheet' id='google-fonts-style-css'  href='https://fonts.googleapis.com/css?family=Merriweather+Sans%3A400%2C700%2C800%7COpen+Sans%3A300italic%2C400%2C400italic%2C600%2C600italic%2C700%2C800%7CRoboto%3A300%2C400%2C400italic%2C500%2C500italic%2C700%2C900%2C800&#038;ver=9.5' type='text/css' media='all' />
<link rel='stylesheet' id='td-theme-css'  href='https://www.prestitotto.it/wp-content/themes/Newspaper/style.css?ver=9.5' type='text/css' media='all' />
<!--n2css--><script type='text/javascript' src='https://www.prestitotto.it/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript'>
var mejsL10n = {"language":"it","strings":{"mejs.install-flash":"Stai usando un browser che non ha Flash player abilitato o installato. Attiva il tuo plugin Flash player o scarica l'ultima versione da https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Disattiva lo schermo intero","mejs.fullscreen-on":"Vai a tutto schermo","mejs.download-video":"Scarica il video","mejs.fullscreen":"Schermo intero","mejs.time-jump-forward":["Vai avanti di 1 secondo","Salta in avanti di %1 secondi"],"mejs.loop":"Attiva\/disattiva la riproduzione automatica","mejs.play":"Play","mejs.pause":"Pausa","mejs.close":"Chiudi","mejs.time-slider":"Time Slider","mejs.time-help-text":"Usa i tasti freccia sinistra\/destra per avanzare di un secondo, su\/gi\u00f9 per avanzare di 10 secondi.","mejs.time-skip-back":["Torna indietro di 1 secondo","Vai indietro di %1 secondi"],"mejs.captions-subtitles":"Didascalie\/Sottotitoli","mejs.captions-chapters":"Capitoli","mejs.none":"Nessuna","mejs.mute-toggle":"Cambia il muto","mejs.volume-help-text":"Usa i tasti freccia su\/gi\u00f9 per aumentare o diminuire il volume.","mejs.unmute":"Togli il muto","mejs.mute":"Muto","mejs.volume-slider":"Cursore del volume","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.ad-skip":"Salta pubblicit\u00e0","mejs.ad-skip-info":["Salta in 1 secondo","Salta in %1 secondi"],"mejs.source-chooser":"Scelta sorgente","mejs.stop":"Stop","mejs.speed-rate":"Velocit\u00e0 di riproduzione","mejs.live-broadcast":"Diretta streaming","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanese","mejs.arabic":"Arabo","mejs.belarusian":"Bielorusso","mejs.bulgarian":"Bulgaro","mejs.catalan":"Catalano","mejs.chinese":"Cinese","mejs.chinese-simplified":"Cinese (semplificato)","mejs.chinese-traditional":"Cinese (tradizionale)","mejs.croatian":"Croato","mejs.czech":"Ceco","mejs.danish":"Danese","mejs.dutch":"Olandese","mejs.english":"Inglese","mejs.estonian":"Estone","mejs.filipino":"Filippino","mejs.finnish":"Finlandese","mejs.french":"Francese","mejs.galician":"Galician","mejs.german":"Tedesco","mejs.greek":"Greco","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Ebraico","mejs.hindi":"Hindi","mejs.hungarian":"Ungherese","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesiano","mejs.irish":"Irish","mejs.italian":"Italiano","mejs.japanese":"Giapponese","mejs.korean":"Coreano","mejs.latvian":"Lettone","mejs.lithuanian":"Lituano","mejs.macedonian":"Macedone","mejs.malay":"Malese","mejs.maltese":"Maltese","mejs.norwegian":"Norvegese","mejs.persian":"Persiano","mejs.polish":"Polacco","mejs.portuguese":"Portoghese","mejs.romanian":"Romeno","mejs.russian":"Russo","mejs.serbian":"Serbo","mejs.slovak":"Slovak","mejs.slovenian":"Sloveno","mejs.spanish":"Spagnolo","mejs.swahili":"Swahili","mejs.swedish":"Svedese","mejs.tagalog":"Tagalog","mejs.thai":"Thailandese","mejs.turkish":"Turco","mejs.ukrainian":"Ucraino","mejs.vietnamese":"Vietnamita","mejs.welsh":"Gallese","mejs.yiddish":"Yiddish"}};
</script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1'></script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=4.9.14'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<link rel='https://api.w.org/' href='https://www.prestitotto.it/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.prestitotto.it/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.prestitotto.it/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.9.14" />
<style id="ctcc-css" type="text/css" media="screen">
				#catapult-cookie-bar {
					box-sizing: border-box;
					max-height: 0;
					opacity: 0;
					z-index: 99999;
					overflow: hidden;
					color: #ffffff;
					position: fixed;
					left: 0;
					bottom: 0;
					width: 100%;
					background-color: #252e39;
				}
				#catapult-cookie-bar a {
					color: #fff;
				}
				#catapult-cookie-bar .x_close span {
					background-color: ;
				}
				button#catapultCookie {
					background:#39c5f4;
					color: ;
					border: 0; padding: 6px 9px; border-radius: 3px;
				}
				#catapult-cookie-bar h3 {
					color: #ffffff;
				}
				.has-cookie-bar #catapult-cookie-bar {
					opacity: 1;
					max-height: 999px;
					min-height: 30px;
				}</style>	<script>
		//apply keyword style only if keywords are Fetched
		jQuery(document).on("keywordsFetched",function(){
			jQuery(".bluet_tooltip").each(function(){

//console.log(jQuery(this).prop("tagName"));

				if(jQuery(this).prop("tagName")!="IMG"){
					jQuery(this).css({
						"text-decoration": "none",
						"color": "#005689",

						"background": "","padding": "1px 5px 3px 5px","font-size": "1em"					});
				}

			});
		});
	</script>

	<style>
	/*for alt images tooltips*/
	.bluet_tooltip_alt{
		color: #ffffff  !important;
		background-color: #005689  !important;
	}



	.bluet_block_to_show{
		max-width: 400px;
	}
	.bluet_block_container{
		color: #ffffff  !important;
		background: #005689  !important;
		box-shadow: 0px 0px 10px #717171  !important;
		font-size:22px  !important;
	}

	img.bluet_tooltip {
	  /*border: none;
	  width:22px;*/
	}

	.kttg_arrow_show_bottom:after{
		border-bottom-color: #005689;
	}

	.kttg_arrow_show_top:after{
		border-top-color: #005689;
	}

	.kttg_arrow_show_right:after{
		border-top-color: #005689;
	}

	.kttg_arrow_show_left:after{
		border-top-color: #005689;
	}

	@media screen and (max-width:400px){
		.bluet_hide_tooltip_button{
		    color: #ffffff  !important;
		    /*background-color: #005689  !important;*/
		}
	}
	</style>
	<script type="text/javascript" src="https://www.prestitotto.it/wp-content/plugins/wp-tooltipy/advanced/assets/findandreplacedomtext.js"></script>			<script>
				window.tdwGlobal = {"adminUrl":"https:\/\/www.prestitotto.it\/wp-admin\/","wpRestNonce":"00d23f2869","wpRestUrl":"https:\/\/www.prestitotto.it\/wp-json\/","permalinkStructure":"\/%year%\/%monthnum%\/%day%\/%postname%\/"};
			</script>
			<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->

<!-- Facebook Pixel Code -->
<script type='text/javascript'>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
</script>
<!-- End Facebook Pixel Code -->
<script type='text/javascript'>
  fbq('init', '758740244489661', [], {
    "agent": "wordpress-4.9.14-1.7.21"
});
</script><script type='text/javascript'>
  fbq('track', 'PageView', []);
</script>
<!-- Facebook Pixel Code -->
<noscript>
<img height="1" width="1" style="display:none" alt="fbpx"
src="https://www.facebook.com/tr?id=758740244489661&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->

<!-- JS generated by theme -->

<script>



	    var tdBlocksArray = []; //here we store all the items for the current page

	    //td_block class - each ajax block uses a object of this class for requests
	    function tdBlock() {
		    this.id = '';
		    this.block_type = 1; //block type id (1-234 etc)
		    this.atts = '';
		    this.td_column_number = '';
		    this.td_current_page = 1; //
		    this.post_count = 0; //from wp
		    this.found_posts = 0; //from wp
		    this.max_num_pages = 0; //from wp
		    this.td_filter_value = ''; //current live filter value
		    this.is_ajax_running = false;
		    this.td_user_action = ''; // load more or infinite loader (used by the animation)
		    this.header_color = '';
		    this.ajax_pagination_infinite_stop = ''; //show load more at page x
	    }


        // td_js_generator - mini detector
        (function(){
            var htmlTag = document.getElementsByTagName("html")[0];

	        if ( navigator.userAgent.indexOf("MSIE 10.0") > -1 ) {
                htmlTag.className += ' ie10';
            }

            if ( !!navigator.userAgent.match(/Trident.*rv\:11\./) ) {
                htmlTag.className += ' ie11';
            }

	        if ( navigator.userAgent.indexOf("Edge") > -1 ) {
                htmlTag.className += ' ieEdge';
            }

            if ( /(iPad|iPhone|iPod)/g.test(navigator.userAgent) ) {
                htmlTag.className += ' td-md-is-ios';
            }

            var user_agent = navigator.userAgent.toLowerCase();
            if ( user_agent.indexOf("android") > -1 ) {
                htmlTag.className += ' td-md-is-android';
            }

            if ( -1 !== navigator.userAgent.indexOf('Mac OS X')  ) {
                htmlTag.className += ' td-md-is-os-x';
            }

            if ( /chrom(e|ium)/.test(navigator.userAgent.toLowerCase()) ) {
               htmlTag.className += ' td-md-is-chrome';
            }

            if ( -1 !== navigator.userAgent.indexOf('Firefox') ) {
                htmlTag.className += ' td-md-is-firefox';
            }

            if ( -1 !== navigator.userAgent.indexOf('Safari') && -1 === navigator.userAgent.indexOf('Chrome') ) {
                htmlTag.className += ' td-md-is-safari';
            }

            if( -1 !== navigator.userAgent.indexOf('IEMobile') ){
                htmlTag.className += ' td-md-is-iemobile';
            }

        })();




        var tdLocalCache = {};

        ( function () {
            "use strict";

            tdLocalCache = {
                data: {},
                remove: function (resource_id) {
                    delete tdLocalCache.data[resource_id];
                },
                exist: function (resource_id) {
                    return tdLocalCache.data.hasOwnProperty(resource_id) && tdLocalCache.data[resource_id] !== null;
                },
                get: function (resource_id) {
                    return tdLocalCache.data[resource_id];
                },
                set: function (resource_id, cachedData) {
                    tdLocalCache.remove(resource_id);
                    tdLocalCache.data[resource_id] = cachedData;
                }
            };
        })();



var td_viewport_interval_list=[{"limitBottom":767,"sidebarWidth":228},{"limitBottom":1018,"sidebarWidth":300},{"limitBottom":1140,"sidebarWidth":324}];
var td_animation_stack_effect="type0";
var tds_animation_stack=true;
var td_animation_stack_specific_selectors=".entry-thumb, img";
var td_animation_stack_general_selectors=".td-animation-stack img, .td-animation-stack .entry-thumb, .post img";
var td_ajax_url="https:\/\/www.prestitotto.it\/wp-admin\/admin-ajax.php?td_theme_name=Newspaper&v=9.5";
var td_get_template_directory_uri="https:\/\/www.prestitotto.it\/wp-content\/themes\/Newspaper";
var tds_snap_menu="";
var tds_logo_on_sticky="";
var tds_header_style="";
var td_please_wait="Per favore attendi...";
var td_email_user_pass_incorrect="Utente o password errata!";
var td_email_user_incorrect="Email o Username errati!";
var td_email_incorrect="Email non corretta!";
var tds_more_articles_on_post_enable="";
var tds_more_articles_on_post_time_to_wait="";
var tds_more_articles_on_post_pages_distance_from_top=0;
var tds_theme_color_site_wide="#000000";
var tds_smart_sidebar="enabled";
var tdThemeName="Newspaper";
var td_magnific_popup_translation_tPrev="Precedente (Freccia Sinistra)";
var td_magnific_popup_translation_tNext="Successivo (tasto freccia destra)";
var td_magnific_popup_translation_tCounter="%curr% di %total%";
var td_magnific_popup_translation_ajax_tError="Il contenuto di %url% non pu\u00f2 essere caricato.";
var td_magnific_popup_translation_image_tError="L'immagine #%curr% non pu\u00f2 essere caricata";
var tdDateNamesI18n={"month_names":["gennaio","febbraio","marzo","aprile","maggio","giugno","luglio","agosto","settembre","ottobre","novembre","dicembre"],"month_names_short":["Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dic"],"day_names":["domenica","luned\u00ec","marted\u00ec","mercoled\u00ec","gioved\u00ec","venerd\u00ec","sabato"],"day_names_short":["dom","lun","mar","mer","gio","ven","sab"]};
var td_ad_background_click_link="";
var td_ad_background_click_target="";
</script>


<!-- Header style compiled by theme -->

<style>


body {
	background-color:#ffffff;
}
.td-header-wrap .black-menu .sf-menu > .current-menu-item > a,
    .td-header-wrap .black-menu .sf-menu > .current-menu-ancestor > a,
    .td-header-wrap .black-menu .sf-menu > .current-category-ancestor > a,
    .td-header-wrap .black-menu .sf-menu > li > a:hover,
    .td-header-wrap .black-menu .sf-menu > .sfHover > a,
    .td-header-style-12 .td-header-menu-wrap-full,
    .sf-menu > .current-menu-item > a:after,
    .sf-menu > .current-menu-ancestor > a:after,
    .sf-menu > .current-category-ancestor > a:after,
    .sf-menu > li:hover > a:after,
    .sf-menu > .sfHover > a:after,
    .td-header-style-12 .td-affix,
    .header-search-wrap .td-drop-down-search:after,
    .header-search-wrap .td-drop-down-search .btn:hover,
    input[type=submit]:hover,
    .td-read-more a,
    .td-post-category:hover,
    .td-grid-style-1.td-hover-1 .td-big-grid-post:hover .td-post-category,
    .td-grid-style-5.td-hover-1 .td-big-grid-post:hover .td-post-category,
    .td_top_authors .td-active .td-author-post-count,
    .td_top_authors .td-active .td-author-comments-count,
    .td_top_authors .td_mod_wrap:hover .td-author-post-count,
    .td_top_authors .td_mod_wrap:hover .td-author-comments-count,
    .td-404-sub-sub-title a:hover,
    .td-search-form-widget .wpb_button:hover,
    .td-rating-bar-wrap div,
    .td_category_template_3 .td-current-sub-category,
    .dropcap,
    .td_wrapper_video_playlist .td_video_controls_playlist_wrapper,
    .wpb_default,
    .wpb_default:hover,
    .td-left-smart-list:hover,
    .td-right-smart-list:hover,
    .woocommerce-checkout .woocommerce input.button:hover,
    .woocommerce-page .woocommerce a.button:hover,
    .woocommerce-account div.woocommerce .button:hover,
    #bbpress-forums button:hover,
    .bbp_widget_login .button:hover,
    .td-footer-wrapper .td-post-category,
    .td-footer-wrapper .widget_product_search input[type="submit"]:hover,
    .woocommerce .product a.button:hover,
    .woocommerce .product #respond input#submit:hover,
    .woocommerce .checkout input#place_order:hover,
    .woocommerce .woocommerce.widget .button:hover,
    .single-product .product .summary .cart .button:hover,
    .woocommerce-cart .woocommerce table.cart .button:hover,
    .woocommerce-cart .woocommerce .shipping-calculator-form .button:hover,
    .td-next-prev-wrap a:hover,
    .td-load-more-wrap a:hover,
    .td-post-small-box a:hover,
    .page-nav .current,
    .page-nav:first-child > div,
    .td_category_template_8 .td-category-header .td-category a.td-current-sub-category,
    .td_category_template_4 .td-category-siblings .td-category a:hover,
    #bbpress-forums .bbp-pagination .current,
    #bbpress-forums #bbp-single-user-details #bbp-user-navigation li.current a,
    .td-theme-slider:hover .slide-meta-cat a,
    a.vc_btn-black:hover,
    .td-trending-now-wrapper:hover .td-trending-now-title,
    .td-scroll-up,
    .td-smart-list-button:hover,
    .td-weather-information:before,
    .td-weather-week:before,
    .td_block_exchange .td-exchange-header:before,
    .td_block_big_grid_9.td-grid-style-1 .td-post-category,
    .td_block_big_grid_9.td-grid-style-5 .td-post-category,
    .td-grid-style-6.td-hover-1 .td-module-thumb:after,
    .td-pulldown-syle-2 .td-subcat-dropdown ul:after,
    .td_block_template_9 .td-block-title:after,
    .td_block_template_15 .td-block-title:before,
    div.wpforms-container .wpforms-form div.wpforms-submit-container button[type=submit] {
        background-color: #000000;
    }

    .td_block_template_4 .td-related-title .td-cur-simple-item:before {
        border-color: #000000 transparent transparent transparent !important;
    }

    .woocommerce .woocommerce-message .button:hover,
    .woocommerce .woocommerce-error .button:hover,
    .woocommerce .woocommerce-info .button:hover {
        background-color: #000000 !important;
    }


    .td_block_template_4 .td-related-title .td-cur-simple-item,
    .td_block_template_3 .td-related-title .td-cur-simple-item,
    .td_block_template_9 .td-related-title:after {
        background-color: #000000;
    }

    .woocommerce .product .onsale,
    .woocommerce.widget .ui-slider .ui-slider-handle {
        background: none #000000;
    }

    .woocommerce.widget.widget_layered_nav_filters ul li a {
        background: none repeat scroll 0 0 #000000 !important;
    }

    a,
    cite a:hover,
    .td_mega_menu_sub_cats .cur-sub-cat,
    .td-mega-span h3 a:hover,
    .td_mod_mega_menu:hover .entry-title a,
    .header-search-wrap .result-msg a:hover,
    .td-header-top-menu .td-drop-down-search .td_module_wrap:hover .entry-title a,
    .td-header-top-menu .td-icon-search:hover,
    .td-header-wrap .result-msg a:hover,
    .top-header-menu li a:hover,
    .top-header-menu .current-menu-item > a,
    .top-header-menu .current-menu-ancestor > a,
    .top-header-menu .current-category-ancestor > a,
    .td-social-icon-wrap > a:hover,
    .td-header-sp-top-widget .td-social-icon-wrap a:hover,
    .td-page-content blockquote p,
    .td-post-content blockquote p,
    .mce-content-body blockquote p,
    .comment-content blockquote p,
    .wpb_text_column blockquote p,
    .td_block_text_with_title blockquote p,
    .td_module_wrap:hover .entry-title a,
    .td-subcat-filter .td-subcat-list a:hover,
    .td-subcat-filter .td-subcat-dropdown a:hover,
    .td_quote_on_blocks,
    .dropcap2,
    .dropcap3,
    .td_top_authors .td-active .td-authors-name a,
    .td_top_authors .td_mod_wrap:hover .td-authors-name a,
    .td-post-next-prev-content a:hover,
    .author-box-wrap .td-author-social a:hover,
    .td-author-name a:hover,
    .td-author-url a:hover,
    .td_mod_related_posts:hover h3 > a,
    .td-post-template-11 .td-related-title .td-related-left:hover,
    .td-post-template-11 .td-related-title .td-related-right:hover,
    .td-post-template-11 .td-related-title .td-cur-simple-item,
    .td-post-template-11 .td_block_related_posts .td-next-prev-wrap a:hover,
    .comment-reply-link:hover,
    .logged-in-as a:hover,
    #cancel-comment-reply-link:hover,
    .td-search-query,
    .td-category-header .td-pulldown-category-filter-link:hover,
    .td-category-siblings .td-subcat-dropdown a:hover,
    .td-category-siblings .td-subcat-dropdown a.td-current-sub-category,
    .widget a:hover,
    .td_wp_recentcomments a:hover,
    .archive .widget_archive .current,
    .archive .widget_archive .current a,
    .widget_calendar tfoot a:hover,
    .woocommerce a.added_to_cart:hover,
    .woocommerce-account .woocommerce-MyAccount-navigation a:hover,
    #bbpress-forums li.bbp-header .bbp-reply-content span a:hover,
    #bbpress-forums .bbp-forum-freshness a:hover,
    #bbpress-forums .bbp-topic-freshness a:hover,
    #bbpress-forums .bbp-forums-list li a:hover,
    #bbpress-forums .bbp-forum-title:hover,
    #bbpress-forums .bbp-topic-permalink:hover,
    #bbpress-forums .bbp-topic-started-by a:hover,
    #bbpress-forums .bbp-topic-started-in a:hover,
    #bbpress-forums .bbp-body .super-sticky li.bbp-topic-title .bbp-topic-permalink,
    #bbpress-forums .bbp-body .sticky li.bbp-topic-title .bbp-topic-permalink,
    .widget_display_replies .bbp-author-name,
    .widget_display_topics .bbp-author-name,
    .footer-text-wrap .footer-email-wrap a,
    .td-subfooter-menu li a:hover,
    .footer-social-wrap a:hover,
    a.vc_btn-black:hover,
    .td-smart-list-dropdown-wrap .td-smart-list-button:hover,
    .td_module_17 .td-read-more a:hover,
    .td_module_18 .td-read-more a:hover,
    .td_module_19 .td-post-author-name a:hover,
    .td-instagram-user a,
    .td-pulldown-syle-2 .td-subcat-dropdown:hover .td-subcat-more span,
    .td-pulldown-syle-2 .td-subcat-dropdown:hover .td-subcat-more i,
    .td-pulldown-syle-3 .td-subcat-dropdown:hover .td-subcat-more span,
    .td-pulldown-syle-3 .td-subcat-dropdown:hover .td-subcat-more i,
    .td-block-title-wrap .td-wrapper-pulldown-filter .td-pulldown-filter-display-option:hover,
    .td-block-title-wrap .td-wrapper-pulldown-filter .td-pulldown-filter-display-option:hover i,
    .td-block-title-wrap .td-wrapper-pulldown-filter .td-pulldown-filter-link:hover,
    .td-block-title-wrap .td-wrapper-pulldown-filter .td-pulldown-filter-item .td-cur-simple-item,
    .td_block_template_2 .td-related-title .td-cur-simple-item,
    .td_block_template_5 .td-related-title .td-cur-simple-item,
    .td_block_template_6 .td-related-title .td-cur-simple-item,
    .td_block_template_7 .td-related-title .td-cur-simple-item,
    .td_block_template_8 .td-related-title .td-cur-simple-item,
    .td_block_template_9 .td-related-title .td-cur-simple-item,
    .td_block_template_10 .td-related-title .td-cur-simple-item,
    .td_block_template_11 .td-related-title .td-cur-simple-item,
    .td_block_template_12 .td-related-title .td-cur-simple-item,
    .td_block_template_13 .td-related-title .td-cur-simple-item,
    .td_block_template_14 .td-related-title .td-cur-simple-item,
    .td_block_template_15 .td-related-title .td-cur-simple-item,
    .td_block_template_16 .td-related-title .td-cur-simple-item,
    .td_block_template_17 .td-related-title .td-cur-simple-item,
    .td-theme-wrap .sf-menu ul .td-menu-item > a:hover,
    .td-theme-wrap .sf-menu ul .sfHover > a,
    .td-theme-wrap .sf-menu ul .current-menu-ancestor > a,
    .td-theme-wrap .sf-menu ul .current-category-ancestor > a,
    .td-theme-wrap .sf-menu ul .current-menu-item > a,
    .td_outlined_btn {
        color: #000000;
    }

    a.vc_btn-black.vc_btn_square_outlined:hover,
    a.vc_btn-black.vc_btn_outlined:hover,
    .td-mega-menu-page .wpb_content_element ul li a:hover,
    .td-theme-wrap .td-aj-search-results .td_module_wrap:hover .entry-title a,
    .td-theme-wrap .header-search-wrap .result-msg a:hover {
        color: #000000 !important;
    }

    .td-next-prev-wrap a:hover,
    .td-load-more-wrap a:hover,
    .td-post-small-box a:hover,
    .page-nav .current,
    .page-nav:first-child > div,
    .td_category_template_8 .td-category-header .td-category a.td-current-sub-category,
    .td_category_template_4 .td-category-siblings .td-category a:hover,
    #bbpress-forums .bbp-pagination .current,
    .post .td_quote_box,
    .page .td_quote_box,
    a.vc_btn-black:hover,
    .td_block_template_5 .td-block-title > *,
    .td_outlined_btn {
        border-color: #000000;
    }

    .td_wrapper_video_playlist .td_video_currently_playing:after {
        border-color: #000000 !important;
    }

    .header-search-wrap .td-drop-down-search:before {
        border-color: transparent transparent #000000 transparent;
    }

    .block-title > span,
    .block-title > a,
    .block-title > label,
    .widgettitle,
    .widgettitle:after,
    .td-trending-now-title,
    .td-trending-now-wrapper:hover .td-trending-now-title,
    .wpb_tabs li.ui-tabs-active a,
    .wpb_tabs li:hover a,
    .vc_tta-container .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic .vc_tta-tabs-container .vc_tta-tab.vc_active > a,
    .vc_tta-container .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic .vc_tta-tabs-container .vc_tta-tab:hover > a,
    .td_block_template_1 .td-related-title .td-cur-simple-item,
    .woocommerce .product .products h2:not(.woocommerce-loop-product__title),
    .td-subcat-filter .td-subcat-dropdown:hover .td-subcat-more,
    .td_3D_btn,
    .td_shadow_btn,
    .td_default_btn,
    .td_round_btn,
    .td_outlined_btn:hover {
    	background-color: #000000;
    }

    .woocommerce div.product .woocommerce-tabs ul.tabs li.active {
    	background-color: #000000 !important;
    }

    .block-title,
    .td_block_template_1 .td-related-title,
    .wpb_tabs .wpb_tabs_nav,
    .vc_tta-container .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic .vc_tta-tabs-container,
    .woocommerce div.product .woocommerce-tabs ul.tabs:before {
        border-color: #000000;
    }
    .td_block_wrap .td-subcat-item a.td-cur-simple-item {
	    color: #000000;
	}



    .td-grid-style-4 .entry-title
    {
        background-color: rgba(0, 0, 0, 0.7);
    }


    .td-header-top-menu,
    .td-header-top-menu a,
    .td-header-wrap .td-header-top-menu-full .td-header-top-menu,
    .td-header-wrap .td-header-top-menu-full a,
    .td-header-style-8 .td-header-top-menu,
    .td-header-style-8 .td-header-top-menu a,
    .td-header-top-menu .td-drop-down-search .entry-title a {
        color: #ffffff;
    }


    .sf-menu > .current-menu-item > a:after,
    .sf-menu > .current-menu-ancestor > a:after,
    .sf-menu > .current-category-ancestor > a:after,
    .sf-menu > li:hover > a:after,
    .sf-menu > .sfHover > a:after,
    .td_block_mega_menu .td-next-prev-wrap a:hover,
    .td-mega-span .td-post-category:hover,
    .td-header-wrap .black-menu .sf-menu > li > a:hover,
    .td-header-wrap .black-menu .sf-menu > .current-menu-ancestor > a,
    .td-header-wrap .black-menu .sf-menu > .sfHover > a,
    .header-search-wrap .td-drop-down-search:after,
    .header-search-wrap .td-drop-down-search .btn:hover,
    .td-header-wrap .black-menu .sf-menu > .current-menu-item > a,
    .td-header-wrap .black-menu .sf-menu > .current-menu-ancestor > a,
    .td-header-wrap .black-menu .sf-menu > .current-category-ancestor > a {
        background-color: #005689;
    }


    .td_block_mega_menu .td-next-prev-wrap a:hover {
        border-color: #005689;
    }

    .header-search-wrap .td-drop-down-search:before {
        border-color: transparent transparent #005689 transparent;
    }

    .td_mega_menu_sub_cats .cur-sub-cat,
    .td_mod_mega_menu:hover .entry-title a,
    .td-theme-wrap .sf-menu ul .td-menu-item > a:hover,
    .td-theme-wrap .sf-menu ul .sfHover > a,
    .td-theme-wrap .sf-menu ul .current-menu-ancestor > a,
    .td-theme-wrap .sf-menu ul .current-category-ancestor > a,
    .td-theme-wrap .sf-menu ul .current-menu-item > a {
        color: #005689;
    }



    .td-affix .sf-menu > .current-menu-item > a:after,
    .td-affix .sf-menu > .current-menu-ancestor > a:after,
    .td-affix .sf-menu > .current-category-ancestor > a:after,
    .td-affix .sf-menu > li:hover > a:after,
    .td-affix .sf-menu > .sfHover > a:after,
    .td-header-wrap .td-affix .black-menu .sf-menu > li > a:hover,
    .td-header-wrap .td-affix .black-menu .sf-menu > .current-menu-ancestor > a,
    .td-header-wrap .td-affix .black-menu .sf-menu > .sfHover > a,
    .td-affix  .header-search-wrap .td-drop-down-search:after,
    .td-affix  .header-search-wrap .td-drop-down-search .btn:hover,
    .td-header-wrap .td-affix  .black-menu .sf-menu > .current-menu-item > a,
    .td-header-wrap .td-affix  .black-menu .sf-menu > .current-menu-ancestor > a,
    .td-header-wrap .td-affix  .black-menu .sf-menu > .current-category-ancestor > a {
        background-color: #005689;
    }

    .td-affix  .header-search-wrap .td-drop-down-search:before {
        border-color: transparent transparent #005689 transparent;
    }

    .td-theme-wrap .td-affix .sf-menu ul .td-menu-item > a:hover,
    .td-theme-wrap .td-affix .sf-menu ul .sfHover > a,
    .td-theme-wrap .td-affix .sf-menu ul .current-menu-ancestor > a,
    .td-theme-wrap .td-affix .sf-menu ul .current-category-ancestor > a,
    .td-theme-wrap .td-affix .sf-menu ul .current-menu-item > a {
        color: #005689;
    }



    .td-header-wrap .td-header-menu-wrap .sf-menu > li > a,
    .td-header-wrap .td-header-menu-social .td-social-icon-wrap a,
    .td-header-style-4 .td-header-menu-social .td-social-icon-wrap i,
    .td-header-style-5 .td-header-menu-social .td-social-icon-wrap i,
    .td-header-style-6 .td-header-menu-social .td-social-icon-wrap i,
    .td-header-style-12 .td-header-menu-social .td-social-icon-wrap i,
    .td-header-wrap .header-search-wrap #td-header-search-button .td-icon-search {
        color: #000000;
    }
    .td-header-wrap .td-header-menu-social + .td-search-wrapper #td-header-search-button:before {
      background-color: #000000;
    }


    .td-header-wrap .td-header-menu-wrap.td-affix .sf-menu > li > a,
    .td-header-wrap .td-affix .td-header-menu-social .td-social-icon-wrap a,
    .td-header-style-4 .td-affix .td-header-menu-social .td-social-icon-wrap i,
    .td-header-style-5 .td-affix .td-header-menu-social .td-social-icon-wrap i,
    .td-header-style-6 .td-affix .td-header-menu-social .td-social-icon-wrap i,
    .td-header-style-12 .td-affix .td-header-menu-social .td-social-icon-wrap i,
    .td-header-wrap .td-affix .header-search-wrap .td-icon-search {
        color: #ffffff;
    }
    .td-header-wrap .td-affix .td-header-menu-social + .td-search-wrapper #td-header-search-button:before {
      background-color: #ffffff;
    }


    .td-theme-wrap .header-search-wrap .td-drop-down-search .btn {
        background-color: #ffffff;
    }

    .td-footer-wrapper,
    .td-footer-wrapper .td_block_template_7 .td-block-title > *,
    .td-footer-wrapper .td_block_template_17 .td-block-title,
    .td-footer-wrapper .td-block-title-wrap .td-wrapper-pulldown-filter {
        background-color: #ffffff;
    }


    .td-footer-wrapper,
    .td-footer-wrapper a,
    .td-footer-wrapper .block-title a,
    .td-footer-wrapper .block-title span,
    .td-footer-wrapper .block-title label,
    .td-footer-wrapper .td-excerpt,
    .td-footer-wrapper .td-post-author-name span,
    .td-footer-wrapper .td-post-date,
    .td-footer-wrapper .td-social-style3 .td_social_type a,
    .td-footer-wrapper .td-social-style3,
    .td-footer-wrapper .td-social-style4 .td_social_type a,
    .td-footer-wrapper .td-social-style4,
    .td-footer-wrapper .td-social-style9,
    .td-footer-wrapper .td-social-style10,
    .td-footer-wrapper .td-social-style2 .td_social_type a,
    .td-footer-wrapper .td-social-style8 .td_social_type a,
    .td-footer-wrapper .td-social-style2 .td_social_type,
    .td-footer-wrapper .td-social-style8 .td_social_type,
    .td-footer-template-13 .td-social-name,
    .td-footer-wrapper .td_block_template_7 .td-block-title > * {
        color: #293133;
    }

    .td-footer-wrapper .widget_calendar th,
    .td-footer-wrapper .widget_calendar td,
    .td-footer-wrapper .td-social-style2 .td_social_type .td-social-box,
    .td-footer-wrapper .td-social-style8 .td_social_type .td-social-box,
    .td-social-style-2 .td-icon-font:after {
        border-color: #293133;
    }

    .td-footer-wrapper .td-module-comments a,
    .td-footer-wrapper .td-post-category,
    .td-footer-wrapper .td-slide-meta .td-post-author-name span,
    .td-footer-wrapper .td-slide-meta .td-post-date {
        color: #fff;
    }


    .td-footer-bottom-full .td-container::before {
        background-color: rgba(41, 49, 51, 0.1);
    }


    .td-sub-footer-container {
        background-color: rgba(0,86,137,0.01);
    }


    .td-sub-footer-container,
    .td-subfooter-menu li a {
        color: #ffffff;
    }


    .top-header-menu > li > a,
    .td-weather-top-widget .td-weather-now .td-big-degrees,
    .td-weather-top-widget .td-weather-header .td-weather-city,
    .td-header-sp-top-menu .td_data_time {
        font-family:"Merriweather Sans";
	font-size:14px;
	line-height:30px;
	text-transform:lowercase;

    }

    .top-header-menu .menu-item-has-children li a {
    	font-family:"Merriweather Sans";

    }

    ul.sf-menu > .td-menu-item > a,
    .td-theme-wrap .td-header-menu-social {
        font-family:"Merriweather Sans";
	font-size:14px;
	line-height:30px;
	font-weight:bold;
	text-transform:uppercase;

    }

    .sf-menu ul .td-menu-item a {
        font-family:"Merriweather Sans";

    }

    .td_mod_mega_menu .item-details a {
        font-family:"Merriweather Sans";

    }

    .td_mega_menu_sub_cats .block-mega-child-cats a {
        font-family:"Merriweather Sans";

    }

    .block-title > span,
    .block-title > a,
    .widgettitle,
    .td-trending-now-title,
    .wpb_tabs li a,
    .vc_tta-container .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic .vc_tta-tabs-container .vc_tta-tab > a,
    .td-theme-wrap .td-related-title a,
    .woocommerce div.product .woocommerce-tabs ul.tabs li a,
    .woocommerce .product .products h2:not(.woocommerce-loop-product__title),
    .td-theme-wrap .td-block-title {
        font-family:"Merriweather Sans";
	font-size:14px;
	line-height:12px;
	font-weight:900;
	text-transform:none;

    }

    .td-theme-wrap .td-subcat-filter,
    .td-theme-wrap .td-subcat-filter .td-subcat-dropdown,
    .td-theme-wrap .td-block-title-wrap .td-wrapper-pulldown-filter .td-pulldown-filter-display-option,
    .td-theme-wrap .td-pulldown-category {
        line-height: 12px;
    }
    .td_block_template_1 .block-title > * {
        padding-bottom: 0;
        padding-top: 0;
    }

    .td-big-grid-meta .td-post-category,
    .td_module_wrap .td-post-category,
    .td-module-image .td-post-category {
        font-family:"Merriweather Sans";
	font-weight:normal;
	text-transform:capitalize;

    }

	.td_module_wrap .td-module-title {
		font-family:"Merriweather Sans";

	}

	.td_block_trending_now .entry-title,
	.td-theme-slider .td-module-title,
    .td-big-grid-post .entry-title {
		font-family:"Merriweather Sans";

	}

	.td-sub-footer-menu ul li a {
		font-size:16px;

	}





    .top-header-menu > li,
    .td-header-sp-top-menu,
    #td-outer-wrap .td-header-sp-top-widget .td-search-btns-wrap,
    #td-outer-wrap .td-header-sp-top-widget .td-social-icon-wrap {
        line-height: 30px;
    }


    @media (min-width: 768px) {
        .td-header-style-4 .td-main-menu-logo img,
        .td-header-style-5 .td-main-menu-logo img,
        .td-header-style-6 .td-main-menu-logo img,
        .td-header-style-7 .td-header-sp-logo img,
        .td-header-style-12 .td-main-menu-logo img {
            max-height: 30px;
        }
        .td-header-style-4 .td-main-menu-logo,
        .td-header-style-5 .td-main-menu-logo,
        .td-header-style-6 .td-main-menu-logo,
        .td-header-style-7 .td-header-sp-logo,
        .td-header-style-12 .td-main-menu-logo {
            height: 30px;
        }
        .td-header-style-4 .td-main-menu-logo a,
        .td-header-style-5 .td-main-menu-logo a,
        .td-header-style-6 .td-main-menu-logo a,
        .td-header-style-7 .td-header-sp-logo a,
        .td-header-style-7 .td-header-sp-logo img,
        .td-header-style-7 .header-search-wrap #td-header-search-button .td-icon-search,
        .td-header-style-12 .td-main-menu-logo a,
        .td-header-style-12 .td-header-menu-wrap .sf-menu > li > a {
            line-height: 30px;
        }
        .td-header-style-7 .sf-menu,
        .td-header-style-7 .td-header-menu-social {
            margin-top: 0;
        }
        .td-header-style-7 #td-top-search {
            top: 0;
            bottom: 0;
        }
        .header-search-wrap #td-header-search-button .td-icon-search {
            line-height: 30px;
        }
    }
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130067216-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-130067216-1');
</script><script type="application/ld+json">
                        {
                            "@context": "http://schema.org",
                            "@type": "BreadcrumbList",
                            "itemListElement": [{
                            "@type": "ListItem",
                            "position": 1,
                                "item": {
                                "@type": "WebSite",
                                "@id": "https://www.prestitotto.it/",
                                "name": "Home"
                            }
                        },{
                            "@type": "ListItem",
                            "position": 2,
                                "item": {
                                "@type": "WebPage",
                                "@id": "https://www.prestitotto.it/category/attualita/",
                                "name": "Attualità"
                            }
                        }    ]
                        }
                       </script>
<!-- Button style compiled by theme -->

<style>
    .tdm-menu-active-style3 .tdm-header.td-header-wrap .sf-menu > .current-category-ancestor > a,
                .tdm-menu-active-style3 .tdm-header.td-header-wrap .sf-menu > .current-menu-ancestor > a,
                .tdm-menu-active-style3 .tdm-header.td-header-wrap .sf-menu > .current-menu-item > a,
                .tdm-menu-active-style3 .tdm-header.td-header-wrap .sf-menu > .sfHover > a,
                .tdm-menu-active-style3 .tdm-header.td-header-wrap .sf-menu > li > a:hover,
                .tdm_block_column_content:hover .tdm-col-content-title-url .tdm-title,
                .tds-button2 .tdm-btn-text,
                .tds-button2 i,
                .tds-button5:hover .tdm-btn-text,
                .tds-button5:hover i,
                .tds-button6 .tdm-btn-text,
                .tds-button6 i,
                .tdm_block_list .tdm-list-item i,
                .tdm_block_pricing .tdm-pricing-feature i,
                .tdm-social-item i {
                  color: #000000;
                }
                .tdm-menu-active-style5 .td-header-menu-wrap .sf-menu > .current-menu-item > a,
                .tdm-menu-active-style5 .td-header-menu-wrap .sf-menu > .current-menu-ancestor > a,
                .tdm-menu-active-style5 .td-header-menu-wrap .sf-menu > .current-category-ancestor > a,
                .tdm-menu-active-style5 .td-header-menu-wrap .sf-menu > li > a:hover,
                .tdm-menu-active-style5 .td-header-menu-wrap .sf-menu > .sfHover > a,
                .tds-button1,
                .tds-button6:after,
                .tds-title2 .tdm-title-line:after,
                .tds-title3 .tdm-title-line:after,
                .tdm_block_pricing.tdm-pricing-featured:before,
                .tdm_block_pricing.tds_pricing2_block.tdm-pricing-featured .tdm-pricing-header,
                .tds-progress-bar1 .tdm-progress-bar:after,
                .tds-progress-bar2 .tdm-progress-bar:after,
                .tds-social3 .tdm-social-item {
                  background-color: #000000;
                }
                .tdm-menu-active-style4 .tdm-header .sf-menu > .current-menu-item > a,
                .tdm-menu-active-style4 .tdm-header .sf-menu > .current-menu-ancestor > a,
                .tdm-menu-active-style4 .tdm-header .sf-menu > .current-category-ancestor > a,
                .tdm-menu-active-style4 .tdm-header .sf-menu > li > a:hover,
                .tdm-menu-active-style4 .tdm-header .sf-menu > .sfHover > a,
                .tds-button2:before,
                .tds-button6:before,
                .tds-progress-bar3 .tdm-progress-bar:after {
                  border-color: #000000;
                }
                .tdm-btn-style1 {
					background-color: #000000;
				}
				.tdm-btn-style2:before {
				    border-color: #000000;
				}
				.tdm-btn-style2 {
				    color: #000000;
				}
				.tdm-btn-style3 {
				    -webkit-box-shadow: 0 2px 16px #000000;
                    -moz-box-shadow: 0 2px 16px #000000;
                    box-shadow: 0 2px 16px #000000;
				}
				.tdm-btn-style3:hover {
				    -webkit-box-shadow: 0 4px 26px #000000;
                    -moz-box-shadow: 0 4px 26px #000000;
                    box-shadow: 0 4px 26px #000000;
				}


                .tdm-menu-active-style3 .tdm-header.td-header-wrap .sf-menu > .current-menu-item > a,
                .tdm-menu-active-style3 .tdm-header.td-header-wrap .sf-menu > .current-menu-ancestor > a,
                .tdm-menu-active-style3 .tdm-header.td-header-wrap .sf-menu > .current-category-ancestor > a,
                .tdm-menu-active-style3 .tdm-header.td-header-wrap .sf-menu > li > a:hover,
                .tdm-menu-active-style3 .tdm-header.td-header-wrap .sf-menu > .sfHover > a {
                  color: #005689;
                }
                .tdm-menu-active-style4 .tdm-header .sf-menu > .current-menu-item > a,
                .tdm-menu-active-style4 .tdm-header .sf-menu > .current-menu-ancestor > a,
                .tdm-menu-active-style4 .tdm-header .sf-menu > .current-category-ancestor > a,
                .tdm-menu-active-style4 .tdm-header .sf-menu > li > a:hover,
                .tdm-menu-active-style4 .tdm-header .sf-menu > .sfHover > a {
                  border-color: #005689;
                }
                .tdm-menu-active-style5 .tdm-header .td-header-menu-wrap .sf-menu > .current-menu-item > a,
                .tdm-menu-active-style5 .tdm-header .td-header-menu-wrap .sf-menu > .current-menu-ancestor > a,
                .tdm-menu-active-style5 .tdm-header .td-header-menu-wrap .sf-menu > .current-category-ancestor > a,
                .tdm-menu-active-style5 .tdm-header .td-header-menu-wrap .sf-menu > li > a:hover,
                .tdm-menu-active-style5 .tdm-header .td-header-menu-wrap .sf-menu > .sfHover > a {
                  background-color: #005689;
                }


                .tdm-menu-active-style3 .tdm-header .td-affix .sf-menu > .current-menu-item > a,
                .tdm-menu-active-style3 .tdm-header .td-affix .sf-menu > .current-menu-ancestor > a,
                .tdm-menu-active-style3 .tdm-header .td-affix .sf-menu > .current-category-ancestor > a,
                .tdm-menu-active-style3 .tdm-header .td-affix .sf-menu > li > a:hover,
                .tdm-menu-active-style3 .tdm-header .td-affix .sf-menu > .sfHover > a {
                  color: #005689;
                }
                .tdm-menu-active-style4 .tdm-header .td-affix .sf-menu > .current-menu-item > a,
                .tdm-menu-active-style4 .tdm-header .td-affix .sf-menu > .current-menu-ancestor > a,
                .tdm-menu-active-style4 .tdm-header .td-affix .sf-menu > .current-category-ancestor > a,
                .tdm-menu-active-style4 .tdm-header .td-affix .sf-menu > li > a:hover,
                .tdm-menu-active-style4 .tdm-header .td-affix .sf-menu > .sfHover > a {
                  border-color: #005689;
                }
                .tdm-menu-active-style5 .tdm-header .td-header-menu-wrap.td-affix .sf-menu > .current-menu-item > a,
                .tdm-menu-active-style5 .tdm-header .td-header-menu-wrap.td-affix .sf-menu > .current-menu-ancestor > a,
                .tdm-menu-active-style5 .tdm-header .td-header-menu-wrap.td-affix .sf-menu > .current-category-ancestor > a,
                .tdm-menu-active-style5 .tdm-header .td-header-menu-wrap.td-affix .sf-menu > li > a:hover,
                .tdm-menu-active-style5 .tdm-header .td-header-menu-wrap.td-affix .sf-menu > .sfHover > a {
                  background-color: #005689;
                }



                .tdm-menu-active-style2 .tdm-header ul.sf-menu > .td-menu-item,
                .tdm-menu-active-style4 .tdm-header ul.sf-menu > .td-menu-item,
                .tdm-header .tdm-header-menu-btns,
                .tdm-header-style-1 .td-main-menu-logo a,
                .tdm-header-style-2 .td-main-menu-logo a,
                .tdm-header-style-3 .td-main-menu-logo a,
                .tdm-header-style-1 .td-header-menu-wrap-full #td-header-search-button .td-icon-search,
                .tdm-header-style-2 .td-header-menu-wrap-full #td-header-search-button .td-icon-search,
                .tdm-header-style-3 .td-header-menu-wrap-full #td-header-search-button .td-icon-search {
                    line-height: 30px;
                }
                .tdm-header-style-1 .td-main-menu-logo,
                .tdm-header-style-2 .td-main-menu-logo,
                .tdm-header-style-3 .td-main-menu-logo {
                    height: 30px;
                }
                @media (min-width: 767px) {
                    .tdm-header-style-1 .td-main-menu-logo img,
                    .tdm-header-style-2 .td-main-menu-logo img,
                    .tdm-header-style-3 .td-main-menu-logo img {
                        max-height: 30px;
                    }
                }
</style>

	<style id="tdw-css-placeholder"></style></head>

<body class="archive category category-attualita category-2327 global-block-template-1 td_category_template_1 td_category_top_posts_style_disable td-animation-stack-type0 td-boxed-layout" itemscope="itemscope" itemtype="https://schema.org/WebPage">

        <div class="td-scroll-up"><i class="td-icon-menu-up"></i></div>

    <div class="td-menu-background"></div>
<div id="td-mobile-nav">
    <div class="td-mobile-container">
        <!-- mobile menu top section -->
        <div class="td-menu-socials-wrap">
            <!-- socials -->
            <div class="td-menu-socials">

        <span class="td-social-icon-wrap">
            <a target="_blank" href="https://www.facebook.com/Prestitotto-860001687522393/" title="Facebook">
                <i class="td-icon-font td-icon-facebook"></i>
            </a>
        </span>
        <span class="td-social-icon-wrap">
            <a target="_blank" href="https://www.linkedin.com/company/prestitottosrl/" title="Linkedin">
                <i class="td-icon-font td-icon-linkedin"></i>
            </a>
        </span>            </div>
            <!-- close button -->
            <div class="td-mobile-close">
                <a href="#"><i class="td-icon-close-mobile"></i></a>
            </div>
        </div>

        <!-- login section -->

        <!-- menu section -->
        <div class="td-mobile-content">
            <div class="menu-menu-16-04-container"><ul id="menu-menu-16-04" class="td-mobile-main-menu"><li id="menu-item-33745" class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-first menu-item-33745"><a href="https://www.prestitotto.it/category/attualita/">Attualità</a></li>
<li id="menu-item-33749" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-33749"><a href="https://www.prestitotto.it/category/motori/">Motori</a></li>
<li id="menu-item-33746" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-33746"><a href="https://www.prestitotto.it/category/casa/">Casa</a></li>
<li id="menu-item-33748" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-33748"><a href="https://www.prestitotto.it/category/prestiti/">Prestiti</a></li>
<li id="menu-item-33744" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-33744"><a href="https://www.prestitotto.it/category/assicurazioni/">Assicurazioni</a></li>
<li id="menu-item-33750" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-33750"><a href="https://www.prestitotto.it/category/salute-e-benessere/">Salute e benessere</a></li>
<li id="menu-item-33747" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-33747"><a href="https://www.prestitotto.it/category/educazione-finanziaria/">Educazione Finanziaria</a></li>
<li id="menu-item-33751" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-33751"><a href="https://www.prestitotto.it/category/utilita/">Utilità</a></li>
</ul></div>        </div>
    </div>

    <!-- register/login section -->
    </div>    <div class="td-search-background"></div>
<div class="td-search-wrap-mob">
	<div class="td-drop-down-search" aria-labelledby="td-header-search-button">
		<form method="get" class="td-search-form" action="https://www.prestitotto.it/">
			<!-- close button -->
			<div class="td-search-close">
				<a href="#"><i class="td-icon-close-mobile"></i></a>
			</div>
			<div role="search" class="td-search-input">
				<span>Cerca</span>
				<input id="td-header-search-mob" type="text" value="" name="s" autocomplete="off" />
			</div>
		</form>
		<div id="td-aj-search-mob"></div>
	</div>
</div>

    <div id="td-outer-wrap" class="td-theme-wrap">

        <!--
Header style 1
-->


<div class="td-header-wrap td-header-style-1 ">

  <div class="td-header-top-menu-full td-container-wrap ">
    <div class="td-container td-header-row td-header-top-menu">

    <div class="top-bar-style-1">

<div class="td-header-sp-top-menu">


	        <div class="td_data_time">
            <div >

                venerdì, 22 maggio 2020
            </div>
        </div>
    </div>
        <div class="td-header-sp-top-widget">

    <div class="td-header-sp-top-widget">
    		<span id="data" style="color:#000;font-weight:bold;margin-right:15px;font-size: 16px;">venerdì, 22 maggio 2020</span>
        <span id="social" style="color: #000;font-weight: bold;padding: 5px;padding-left: 50px;margin-left: 20px;font-size:16px;">Seguici</span>
        <span class="td-social-icon-wrap">
            <a target="_blank" href="https://www.facebook.com/Prestitotto-860001687522393/" style="color:#000;margin-left:5px;" title="Facebook">
            <img src="https://www.prestitotto.it/wp-content/uploads/social/fb_logo.png" style="max-width:30px;"/>
          </a>
        </span>
        <span class="td-social-icon-wrap">
           <a target="_blank" href="https://www.linkedin.com/company/prestitottosrl/" style="color:#000;margin-left:5px;max-width:30px;" title="Linkedin">
            <img src="https://www.prestitotto.it/wp-content/uploads/social/in_logo.png" />
          </a>
        </span>    </div>
</div>    </div>

<!-- LOGIN MODAL -->
    </div>
  </div>

  <div class="td-banner-wrap-full td-logo-wrap-full td-container-wrap ">
    <div class="td-container td-header-row td-header-header">
	<div class="td-pb-row">
	<div class="td-pb-span12">
      <div class="td-header-sp-logo" style="padding:0;">
                <a class="td-main-logo" href="https://www.prestitotto.it/">
            <img class="td-retina-data" data-retina="https://www.prestitotto.it/wp-content/uploads/2020/03/Prestitotto_New_Logo.png" src="https://www.prestitotto.it/wp-content/uploads/2020/03/Prestitotto_New_Logo.png" alt="" />
            <span class="td-visual-hidden">Prestitotto</span>
        </a>
          </div>

</div>

   </div>
  </div>

  <div class="td-container-wrap menu-center" style="margin-top:15px;">

    <div class="td-header-menu-wrap td-header-gradient ">
      <div class="td-container td-header-row td-header-main-menu">
        <div id="td-header-menu" role="navigation">

    <div class="td-main-menu-logo td-logo-in-header">
                <img class="td-retina-data" data-retina="https://www.prestitotto.it/wp-content/uploads/2020/03/Prestitotto_New_Logo.png" src="https://www.prestitotto.it/wp-content/uploads/2020/03/Prestitotto_New_Logo.png" alt="" onclick="location.href='https://www.prestitotto.it/'"/>


    <style>

</style>    </div>

</div>

      </div>
    </div>
  </div>
  <div class="td-container  desktop">

	 <div class="td-pb-row">
	 <div class="td-pb-span3">
			<a href="https://www.prestitotto.it/category/video/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/desktop/video_1.jpg"/></a>
		</div>
		<div class="td-pb-span3">
			<a href="https://www.prestitotto.it/category/attualita/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/desktop/attualita_1.jpg"/></a>
		</div>
		<div class="td-pb-span3">
			<a href="https://www.prestitotto.it/category/prestiti/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/desktop/prestiti_1.jpg"/></a>
		</div>
		<div class="td-pb-span3">
			<a href="https://www.prestitotto.it/category/motori/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/desktop/motori_1.jpg"/></a>
		</div>


	 </div>
	  <div class="td-pb-row" style="margin-top:15px;">

		<div class="td-pb-span3">
			<a href="https://www.prestitotto.it/category/casa/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/desktop/casa_1.jpg"/></a>
		</div>
		<div class="td-pb-span3">
			<a href="https://www.prestitotto.it/category/salute-e-benessere/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/desktop/salute-e-benessere_1.jpg"/></a>
		</div>
		<div class="td-pb-span3">
			<a href="https://www.prestitotto.it/category/utilita/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/desktop/utilita_1.jpg"/></a>
		</div>
		<div class="td-pb-span3">
			<a href="https://www.prestitotto.it/category/assicurazioni/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/desktop/assicurazioni_1.jpg"/></a>
		</div>
	 </div>
  </div>
  <div class="td-container  mobile">
	 <div class="td-pb-row ">
	 <div class="td-pb-span3 span">
			<a href="https://www.prestitotto.it/category/video/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/mobile/video_mobile.jpg"/></a>
		</div>
		 <div class="td-pb-span3 span">
			<a href="https://www.prestitotto.it/category/attualita/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/mobile/attualita_mobile.jpg"/></a>
		</div>

		<div class="td-pb-span3 span">
			<a href="https://www.prestitotto.it/category/prestiti/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/mobile/prestiti_mobile.jpg"/></a>
		</div>
		<div class="td-pb-span3 span">
			<a href="https://www.prestitotto.it/category/motori/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/mobile/motori_mobile.jpg"/></a>
		</div>



	 </div>
	  <div class="td-pb-row ">
	  <div class="td-pb-span3 span">
			<a href="https://www.prestitotto.it/category/casa/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/mobile/casa_mobile.jpg"/></a>
		</div>
		<div class="td-pb-span3 span">
			<a href="https://www.prestitotto.it/category/salute-e-benessere/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/mobile/salute-e-benessere_mobile.jpg"/></a>
		</div>


		<div class="td-pb-span3 span">
			<a href="https://www.prestitotto.it/category/utilita/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/mobile/utilita_mobile.jpg"/></a>
		</div>

		<div class="td-pb-span3 span">
			<a href="https://www.prestitotto.it/category/assicurazioni/"><img src="https://www.prestitotto.it/wp-content/uploads/menu/mobile/assicurazioni_mobile.jpg"/></a>
		</div>
	 </div>
  </div>

 </div>
</div>
<style>
.menu-center{margin-bottom:90px;}
@media screen and (max-width: 1199px) {
	.menu-center{margin-bottom:0px;}
}
.span{
	text-align:center;
	width: 25% !important;
	padding-right: 12px !important;
    padding-left: 12px !important;
	float:left !important;
}
.mobile{display:none;}
@media screen and (max-width: 767px) {
  #data{
    display: none;
  }
  #social{
    display: none;
  }
  .mobile{display:block;}
  .desktop{display:none;}

}
</style>

<div class="td-main-content-wrap td-container-wrap">
    <div class="td-container tdc-content-wrap ">
        <div class="td-crumb-container">
            <div class="entry-crumbs"><span><a title="" class="entry-crumb" href="https://www.prestitotto.it/">Home</a></span> <i class="td-icon-right td-bread-sep td-bred-no-url-last"></i> <span class="td-bred-no-url-last">Codici sconto</span></div>        </div>
            <h1 class="entry-title td-page-title">Benvenuto su Codici Sconto Prestitotto</h1>

            <div class="td-pb-row">
              <div class="td-main-content-wrap td-container-wrap">
                <div class="td-pb-span12 td-main-content" role="main">
                  <div class="td-page-content">

                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


                    <p class="codici-sconto-description">
                      Farti risparmiare è ciò che per noi conta di più. Proprio per questo vogliamo fornirti quotidianamente sconti e offerte esclusive per farti vivere tutte le tue passioni.

                      Che tu sia un amante dell’escursionismo e dello sport,
                      che tu voglia device elettronici sempre al top,
                      he tu sia un pioniere di tendenze,
                      noi abbiamo la promozione adatta alle tue esigenze!

                      Tra le offerte che puoi riscattare on line troverai: <b>Amazon</b>, <b>Adidas</b>, <b>Groupon</b>, <b>AliExpress</b>, e tanti altri ancora!
                      Prestitotto trova i codici sconto giusti per te... non ti resta che cliccare!
                    </p>
                    <?php
                    /**
                    * Template Name: Home Admin
                    */
                    include_once( PRESTITOTTO_PATH . 'core/config.php');

                    $codici = new CodiciSconto();
                    $page = $_REQUEST['x'];
                    if (!$page) $page = 1;
                    $limit = 12;
                    $offset = ($page-1) * $limit;
                    $links = (isset( $_REQUEST['links'])) ? $_REQUEST['links'] : 2;

                    $lista_codici = $codici->all($limit, $offset);
                    $totale_codici = $codici->count();

                    $paginator  = new Paginator( '', $page, $limit, $totale_codici );?>
                    <div class="container">
                      <h2 style="text-align:center;">Le nostre migliori offerte per te!</h2>
                      <div class="text-holder">Non perderti i migliori sconti e offerte dei tuoi shop e brand preferiti: scopri subito quelli attivi in pagina!</div>
                      <div class="row">
                        <?php foreach ($lista_codici as $i => $cs) {?>
                          <div class="td-pb-span4" style="margin-bottom:15px;">
                            <div class="card">
                              <img class="card-img-top" src="<?= $cs->logo ?>" alt="Card image cap">
                              <div class="card-body">
                                <h5 class="card-title"><?= $cs->nome ?></h5>
                                <div class="button-center">
                                  <button class="btn btn-primary" data-codice="<?= $cs->codice ?>"
                                    data-url="<?= $cs->link ?>" data-nome="<?= $cs->nome ?>" id="myBtn-<?=$i?>"
                                    onclick="javascript:openModal('myBtn-<?=$i?>');">Scopri Il Codice</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          <? }?>

                          <div id="myModal" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content">
                              <span class="close" onclick="javascript:closeModal();">&times;</span>
                              <div class="card-body" style="position: inherit;">
                                <h5 class="card-title" id="codicenome"></h5>
                                <div id="codice-container">
                                  <input type="text" readonly id="codice" value="">
                                </div>
                                <a href="" target="_blank" id="url"><button class="btn btn-primary">Attiva il Codice</button></a>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                      <div style="text-align: center;">
                        <div style="display: inline-block">
                          <?php echo $paginator->createLinks( $links, 'pagination pagination-sm' ); ?>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>


    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->

<!-- Instagram -->



	<div class="td-footer-wrapper td-footer-page td-container-wrap">
	<p><div id="td_uid_1_5ec7a672d39f2" class="tdc-row"><div class="vc_row td_uid_8_5ec7a672d3a48_rand  wpb_row td-pb-row" >
<style scoped>

/* custom css */
.td_uid_8_5ec7a672d3a48_rand {
                    min-height: 0;
                }
</style><div class="vc_column td_uid_9_5ec7a672d4637_rand  wpb_column vc_column_container tdc-column td-pb-span12"><div class="wpb_wrapper"><div class="wpb_wrapper td_block_empty_space td_block_wrap vc_empty_space td_uid_10_5ec7a672d47d1_rand "  style="height: 10px"></div></div></div></div></div>
		<script>

			jQuery(window).load(function () {
				jQuery('body').find('#td_uid_1_5ec7a672d39f2 .td-element-style').each(function (index, element) {
					jQuery(element).css('opacity', 1);
					return;
				});
			});

		</script>


			<script>

				jQuery(window).ready(function () {

					// We need timeout because the content must be rendered and interpreted on client
					setTimeout(function () {

						var $content = jQuery('body').find('#tdc-live-iframe'),
							refWindow = undefined;

						if ($content.length) {
							$content = $content.contents();
							refWindow = document.getElementById('tdc-live-iframe').contentWindow || document.getElementById('tdc-live-iframe').contentDocument;

						} else {
							$content = jQuery('body');
							refWindow = window;
						}

						$content.find('#td_uid_1_5ec7a672d39f2 .td-element-style').each(function (index, element) {
							jQuery(element).css('opacity', 1);
							return;
						});
					});

				}, 200);
			</script>

			<div id="td_uid_4_5ec7a672d488c" class="tdc-row"><div class="vc_row td_uid_11_5ec7a672d48cc_rand  wpb_row td-pb-row" >
<style scoped>

/* custom css */
.td_uid_11_5ec7a672d48cc_rand {
                    min-height: 0;
                }
/* inline tdc_css att */

.td_uid_11_5ec7a672d48cc_rand{
margin-bottom:0px !important;
}

.td_uid_11_5ec7a672d48cc_rand .td_block_wrap{ text-align:left }

</style><div class="vc_column td_uid_12_5ec7a672d4b95_rand logo_footer wpb_column vc_column_container tdc-column td-pb-span4"><div class="wpb_wrapper"><div class="wpb_wrapper td_block_wrap vc_raw_html td_uid_13_5ec7a672d4d25_rand ">
<style scoped>

/* inline tdc_css att */

.td_uid_13_5ec7a672d4d25_rand{
margin-bottom:0px !important;
}

</style><div class="td-fix-index"><div class="desktop"><a href="https://www.prestitotto.it"><img src="https://www.prestitotto.it/wp-content/uploads/2020/03/Prestitotto_New_Logo.png" style="max-width:180px"/></a></div>
<div class="mobile"><center><a href="https://www.prestitotto.it"><img src="https://www.prestitotto.it/wp-content/uploads/2020/03/Prestitotto_New_Logo.png" style="max-width:180px"/></a>
</center>
</div></div></div></div></div><div class="vc_column td_uid_14_5ec7a672d4e31_rand  wpb_column vc_column_container tdc-column td-pb-span4"><div class="wpb_wrapper"></div></div><div class="vc_column td_uid_15_5ec7a672d4f11_rand  wpb_column vc_column_container tdc-column td-pb-span4"><div class="wpb_wrapper"></div></div></div></div>
		<script>

			jQuery(window).load(function () {
				jQuery('body').find('#td_uid_4_5ec7a672d488c .td-element-style').each(function (index, element) {
					jQuery(element).css('opacity', 1);
					return;
				});
			});

		</script>


			<script>

				jQuery(window).ready(function () {

					// We need timeout because the content must be rendered and interpreted on client
					setTimeout(function () {

						var $content = jQuery('body').find('#tdc-live-iframe'),
							refWindow = undefined;

						if ($content.length) {
							$content = $content.contents();
							refWindow = document.getElementById('tdc-live-iframe').contentWindow || document.getElementById('tdc-live-iframe').contentDocument;

						} else {
							$content = jQuery('body');
							refWindow = window;
						}

						$content.find('#td_uid_4_5ec7a672d488c .td-element-style').each(function (index, element) {
							jQuery(element).css('opacity', 1);
							return;
						});
					});

				}, 200);
			</script>

			<div id="td_uid_9_5ec7a672d4fbe" class="tdc-row"><div class="vc_row td_uid_16_5ec7a672d4ffd_rand  wpb_row td-pb-row" >
<style scoped>

/* custom css */
.td_uid_16_5ec7a672d4ffd_rand {
                    min-height: 0;
                }
</style><div class="vc_column td_uid_17_5ec7a672d5277_rand  wpb_column vc_column_container tdc-column td-pb-span12"><div class="wpb_wrapper"><div class="td_block_wrap td_block_list_menu td_uid_18_5ec7a672d5bb2_rand td-pb-border-top footer_menu td_block_template_1 widget"  data-td-block-uid="td_uid_18_5ec7a672d5bb2" >
<style>

/* inline tdc_css att */

.td_uid_18_5ec7a672d5bb2_rand{
margin-bottom:0px !important;
}

</style>
<style>
/* custom css */
.td_uid_18_5ec7a672d5bb2_rand li {
					display: inline-block;
				}

				.td_uid_18_5ec7a672d5bb2_rand ul {
					margin: 14px 0px 14px 0px;
				}

				.td_uid_18_5ec7a672d5bb2_rand ul li {
					margin-right: 14px;
				}
				.td_uid_18_5ec7a672d5bb2_rand ul li:last-child {
					margin-right: 0;
				}

				.td_uid_18_5ec7a672d5bb2_rand ul {
					text-align: center;
				}



				.td_uid_18_5ec7a672d5bb2_rand li {
					font-size:17px !important;
				}
</style><div class="td-block-title-wrap"></div><div id=td_uid_18_5ec7a672d5bb2 class="td_block_inner td-fix-index"><div class="menu-menu-footer-container"><ul id="menu-menu-footer" class="menu"><li id="menu-item-33382" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-33382"><a href="https://www.prestitotto.it/">Home</a></li>
<li id="menu-item-33385" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-33385"><a href="https://www.prestitotto.it/chi-siamo/">Chi siamo</a></li>
<li id="menu-item-33386" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-33386"><a href="https://www.prestitotto.it/contatti/">Contatti</a></li>
<li id="menu-item-33387" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-33387"><a href="https://www.prestitotto.it/lavora-con-noi/">Lavora con noi</a></li>
<li id="menu-item-33512" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33512"><a href="https://www.prestitotto.it/dicono-di-noi/">Dicono di noi</a></li>
<li id="menu-item-33388" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-33388"><a href="https://www.prestitotto.it/reclami/">Reclami</a></li>
<li id="menu-item-33389" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-33389"><a href="https://www.prestitotto.it/ufficio-stampa/">Ufficio Stampa</a></li>
<li id="menu-item-33793" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-33793"><a href="https://www.prestitotto.it/strumenti-di-calcolo/">Strumenti di calcolo</a></li>
<li id="menu-item-33792" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-33792"><a href="https://www.prestitotto.it/codici-sconto/lista-codici/">Codici Sconto</a></li>
<li id="menu-item-33390" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-33390"><a href="https://www.prestitotto.it/concessionaria-pubblicitaria/">Concessionaria Pubblicitaria</a></li>
<li id="menu-item-33391" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-33391"><a href="https://www.prestitotto.it/cookie-policy/">Cookie Policy</a></li>
<li id="menu-item-33392" class="link_menu_footer menu-item menu-item-type-post_type menu-item-object-page menu-item-33392"><a href="https://www.prestitotto.it/privacy-policy/">Privacy Policy</a></li>
</ul></div></div></div> <!-- ./block --></div></div></div></div>
		<script>

			jQuery(window).load(function () {
				jQuery('body').find('#td_uid_9_5ec7a672d4fbe .td-element-style').each(function (index, element) {
					jQuery(element).css('opacity', 1);
					return;
				});
			});

		</script>


			<script>

				jQuery(window).ready(function () {

					// We need timeout because the content must be rendered and interpreted on client
					setTimeout(function () {

						var $content = jQuery('body').find('#tdc-live-iframe'),
							refWindow = undefined;

						if ($content.length) {
							$content = $content.contents();
							refWindow = document.getElementById('tdc-live-iframe').contentWindow || document.getElementById('tdc-live-iframe').contentDocument;

						} else {
							$content = jQuery('body');
							refWindow = window;
						}

						$content.find('#td_uid_9_5ec7a672d4fbe .td-element-style').each(function (index, element) {
							jQuery(element).css('opacity', 1);
							return;
						});
					});

				}, 200);
			</script>

			<div id="td_uid_11_5ec7a672dc26f" class="tdc-row"><div class="vc_row td_uid_19_5ec7a672dc29a_rand  wpb_row td-pb-row" >
<style scoped>

/* custom css */
.td_uid_19_5ec7a672dc29a_rand {
                    min-height: 0;
                }
</style><div class="vc_column td_uid_20_5ec7a672dc56c_rand  wpb_column vc_column_container tdc-column td-pb-span12">
<style scoped>

/* inline tdc_css att */

.td_uid_20_5ec7a672dc56c_rand{
margin-bottom:5px !important;
}

</style><div class="wpb_wrapper"><div class="wpb_wrapper td_block_wrap vc_raw_html td_uid_21_5ec7a672dc6bb_rand "><div class="td-fix-index"><div class=" td-container-wrap ">

<center><div style="color:#000 !important;font-size: 12px;
    font-family: 'Open Sans', arial, sans-serif;">© Copyright  2020 - Prestitotto s.r.l testata giornalistica online registrata al Tribunale di Napoli autorizzazione n.° 44 del 2010 | Tutti i diritti riservati. | P.IVA 06616231210</div></center>


        </div></div></div></div></div></div></div>
		<script>

			jQuery(window).load(function () {
				jQuery('body').find('#td_uid_11_5ec7a672dc26f .td-element-style').each(function (index, element) {
					jQuery(element).css('opacity', 1);
					return;
				});
			});

		</script>


			<script>

				jQuery(window).ready(function () {

					// We need timeout because the content must be rendered and interpreted on client
					setTimeout(function () {

						var $content = jQuery('body').find('#tdc-live-iframe'),
							refWindow = undefined;

						if ($content.length) {
							$content = $content.contents();
							refWindow = document.getElementById('tdc-live-iframe').contentWindow || document.getElementById('tdc-live-iframe').contentDocument;

						} else {
							$content = jQuery('body');
							refWindow = window;
						}

						$content.find('#td_uid_11_5ec7a672dc26f .td-element-style').each(function (index, element) {
							jQuery(element).css('opacity', 1);
							return;
						});
					});

				}, 200);
			</script>

			</p>
	</div>



</div><!--close td-outer-wrap-->


		<script type="text/javascript">
			/*test*/
		function tltpy_fetch_kws(){
			/*
			array(0) {
}
			*/
			window.kttg_tab=[
			["Zero\\ Coupon\\ Bond",false,false,'','','',0],
			["Yeld",false,false,'','','',0],
			["Yankee\\ Bond",false,false,'','','',0],
			["Warrant",false,false,'','','',0],
			["Wall\\ Street",false,false,'','','',0],
			["Venture\\ Capital",false,false,'','','',0],
			["Vendita\\ allo\\ scoperto",false,false,'','','',0],
			["Variabilità\\ dei\\ prezzi",false,false,'','','',0],
			["Valuta",false,false,'','','',0],
			["Valore\\ nominale",false,false,'','','',0],
			["Valore\\ attuale",false,false,'','','',0],
			["Utili\\ per\\ azione",false,false,'','','',0],
			["Utile\\ netto",false,false,'','','',0],
			["Utile\\ lordo",false,false,'','','',0],
			["Utile\\ ante\\ imposte",false,false,'','','',0],
			["Usura",false,false,'','','',0],
			["Usufrutto",false,false,'','','',0],
			["Unità",false,false,'','','',0],
			["Unit\\ Linked",false,false,'','','',0],
			["Tributo",false,false,'','','',0],
			["Trend",false,false,'','','',0],
			["Treausry\\ Bonds",false,false,'','','',0],
			["Tratta\\ Bancaria",false,false,'','','',0],
			["Titoli\\ Spazzatura",false,false,'','','',0],
			["Titoli\\ di\\ Stato",false,false,'','','',0],
			["Titoli\\ a\\ reddito\\ fisso",false,false,'','','',0],
			["TFR\\ \\(Trattamento\\ di\\ Fine\\ Rapporto\\)",false,false,'','','',0],
			["TEG\\ \\(Tasso\\ Effettivo\\ Globale\\)",false,false,'','','',0],
			["Terzo\\ Mercato",false,false,'','','',0],
			["Tasso\\ zero",false,false,'','','',0],
			["Tasso\\ Variabile",false,false,'','','',0],
			["Tasso\\ Primario ",false,false,'','','',0],
			["Tasso\\ Fisso",false,false,'','','',0],
			["Tasso\\ di\\ sconto",false,false,'','','',0],
			["Tasso\\ di\\ rendimento",false,false,'','','',0],
			["Tasso\\ di\\ premio",false,false,'','','',0],
			["Tasso\\ di\\ interesse\\ indicizzato",false,false,'','','',0],
			["Tasso\\ di\\ inflazione",false,false,'','','',0],
			["Tasso\\ di\\ cambio",false,false,'','','',0],
			["Tasso\\ di\\ base ",false,false,'','','',0],
			["Tasi",false,false,'','','',0],
			["Tasso",false,false,'','','',0],
			["Target\\ Price",false,false,'','','',0],
			["TAN\\ \\(Tasso\\ Annuo\\ Nominale\\)",false,false,'','','',0],
			["TAEG\\ \\(Tasso\\ Annuo\\ Effettivo\\ Globale\\)",false,false,'','','',0],
			["Surroga\\ del\\ Mutuo",false,false,'','','',0],
			["Svalutazione",false,false,'','','',0],
			["Strumenti\\ finanziari",false,false,'','','',0],
			["STRIP",false,false,'','','',0],
			["STRAP",false,false,'','','',0],
			["Stock\\ of\\ Exchange",false,false,'','','',0],
			["Stato\\ Patrimoniale",false,false,'','','',0],
			["Startup",false,false,'','','',0],
			["Stanza\\ di\\ compensazione",false,false,'','','',0],
			["Spese\\ di\\ istruttoria",false,false,'','','',0],
			["Soggetto\\ ad\\ ipoteca",false,false,'','','',0],
			["Società\\ finanziaria\\ di\\ marca",false,false,'','','',0],
			["Società\\ Finanziaria",false,false,'','','',0],
			["Spread",false,false,'','','',0],
			["Spezzettatura",false,false,'','','',0],
			["Spese\\ di\\ istruttoria",false,false,'','','',0],
			["Speculazione",false,false,'','','',0],
			["Sostegno",false,false,'','','',0],
			["Sospensione",false,false,'','','',0],
			["Società\\ di\\ gestione",false,false,'','','',0],
			["Sistemi\\ di\\ scambi\\ organizzati",false,false,'','','',0],
			["Sim",false,false,'','','',0],
			["Sicav",false,false,'','','',0],
			["SIC\\ \\(sistemi\\ informazioni\\ creditizie\\)",false,false,'','','',0],
			["SGR",false,false,'','','',0],
			["SEC",false,false,'','','',0],
			["Scoperto",false,false,'','','',0],
			["Sconto",false,false,'','','',0],
			["Scindibilità",false,false,'','','',0],
			["Scadenza",false,false,'','','',0],
			["Sbarramento",false,false,'','','',0],
			["Saldo\\ Tecnico",false,false,'','','',0],
			["Saldo\\ disponibile",false,false,'','','',0],
			["Saldo",false,false,'','','',0],
			["ROS",false,false,'','','',0],
			["Rogito",false,false,'','','',0],
			["Ritenuta\\ d’acconto",false,false,'','','',0],
			["Risparmio\\ Gestito",false,false,'','','',0],
			["Rischio\\ di\\ tasso\\ di\\ interesse",false,false,'','','',0],
			["Rischio\\ di\\ Impresa",false,false,'','','',0],
			["Rischio\\ di\\ credito",false,false,'','','',0],
			["Rischio\\ Azionario",false,false,'','','',0],
			["RID\\ \\(Rapporti\\ Interbancari\\ Diretti\\)",false,false,'','','',0],
			["Ricavo\\ Marginale",false,false,'','','',0],
			["Rimborso\\ garantito",false,false,'','','',0],
			["Ribor",false,false,'','','',0],
			["Ribilianciamento\\ del\\ portafoglio",false,false,'','','',0],
			["RIBA",false,false,'','','',0],
			["Riapertura\\ delle\\ aste",false,false,'','','',0],
			["Retention\\ Rate",false,false,'','','',0],
			["Retail\\ Banking",false,false,'','','',0],
			["Requisiti\\ Patrimoniali",false,false,'','','',0],
			["Rendita",false,false,'','','',0],
			["Rendimento\\ Effettivo",false,false,'','','',0],
			["Rendimento\\ di\\ Cassa",false,false,'','','',0],
			["Rendiconto\\ Finanziario",false,false,'','','',0],
			["Regolamento\\ di\\ un\\ conto",false,false,'','','',0],
			["Reddito\\ reale",false,false,'','','',0],
			["Reccomendation",false,false,'','','',0],
			["Recessione",false,false,'','','',0],
			["Real\\ Estate",false,false,'','','',0],
			["Rating",false,false,'','','',0],
			["Rateo",false,false,'','','',0],
			["Rata\\ minima",false,false,'','','',0],
			["Rata",false,false,'','','',0],
			["Rapporto\\ Prezzo\\/Utile",false,false,'','','',0],
			["Rapporto\\ di\\ Credito",false,false,'','','',0],
			["Range",false,false,'','','',0],
			["Quotazione",false,false,'','','',0],
			["Quota\\ qualificante",false,false,'','','',0],
			["Quota\\ interessi",false,false,'','','',0],
			["Quota\\ di\\ mercato",false,false,'','','',0],
			["Quota\\ capitale",false,false,'','','',0],
			["Quota\\ azionaria",false,false,'','','',0],
			["Quarto\\ mercato",false,false,'','','',0],
			["Quantitativo",false,false,'','','',0],
			["Qualità\\ degli\\ utili",false,false,'','','',0],
			["Qualità\\ degli\\ investimenti",false,false,'','','',0],
			["Protesto",false,false,'','','',0],
			["Prospetto\\ informativo",false,false,'','','',0],
			["Proposta\\ di\\ negoziazione",false,false,'','','',0],
			["Promotore\\ finanziario",false,false,'','','',0],
			["Prodotti\\ derivati",false,false,'','','',0],
			["Private\\ Banking",false,false,'','','',0],
			["Prime\\ Rate",false,false,'','','',0],
			["Primary\\ Dealer",false,false,'','','',0],
			["Prezzo\\ di\\ Riferimento",false,false,'','','',0],
			["Prezzo\\ di\\ Mercato",false,false,'','','',0],
			["Prezzo\\ Base",false,false,'','','',0],
			["Prezzo\\ a\\ pronti",false,false,'','','',0],
			["Prestito\\ Ponte",false,false,'','','',0],
			["Prestito\\ personale",false,false,'','','',0],
			["Prestito\\ non\\ finalizzato",false,false,'','','',0],
			["Prestito\\ finalizzato",false,false,'','','',0],
			["Premio\\ Netto\\ Danno",false,false,'','','',0],
			["Premio\\ di\\ conversione",false,false,'','','',0],
			["Premio\\ Assicurativo",false,false,'','','',0],
			["Posizione\\ di\\ Cassa",false,false,'','','',0],
			["POS\\ \\(Point\\ Of\\ Sale\\)",false,false,'','','',0],
			["Portafoglio\\ Prudente",false,false,'','','',0],
			["Portafoglio\\ Cauto",false,false,'','','',0],
			["Portafoglio\\ Bilanciato",false,false,'','','',0],
			["Portafoglio",false,false,'','','',0],
			["Porta\\ a\\ porta",false,false,'','','',0],
			["Polizze\\ Rivalutabili",false,false,'','','',0],
			["Polizza\\ Vita",false,false,'','','',0],
			["Polizza\\ United\\ Linked",false,false,'','','',0],
			["Polizza\\ Mista",false,false,'','','',0],
			["Polizza\\ di\\ rendita",false,false,'','','',0],
			["Polizza\\ assicurativa",false,false,'','','',0],
			["Plusvalenza",false,false,'','','',0],
			["Players",false,false,'','','',0],
			["PIP",false,false,'','','',0],
			["PIN\\ \\(Personal\\ Identification\\ Number\\)",false,false,'','','',0],
			["Pignoramento",false,false,'','','',0],
			["PIC",false,false,'','','',0],
			["Piano\\ di\\ ammortamento",false,false,'','','',0],
			["Periodo\\ di\\ franchigia",false,false,'','','',0],
			["Performance",false,false,'','','',0],
			["Penale\\ estinzione\\ anticipata",false,false,'','','',0],
			["Pegno",false,false,'','','',0],
			["Patrimonio",false,false,'','','',0],
			["Passività\\ correnti",false,false,'','','',0],
			["Parere\\ di\\ accettazione",false,false,'','','',0],
			["Pagobancomat",false,false,'','','',0],
			["Payout\\ Ratio",false,false,'','','',0],
			["PAC",false,false,'','','',0],
			["Over\\ the\\ Counter",false,false,'','','',0],
			["Oscillatori",false,false,'','','',0],
			["Ordine\\ di\\ Borsa",false,false,'','','',0],
			["Opzioni",false,false,'','','',0],
			["Operazioni\\ overnight",false,false,'','','',0],
			["Open\\ Interest",false,false,'','','',0],
			["OPA",false,false,'','','',0],
			["Online\\ Broker",false,false,'','','',0],
			["Oneri\\ Tecnici\\ Vita",false,false,'','','',0],
			["Oneri\\ Tecnici\\ Danni",false,false,'','','',0],
			["Obbligo\\ di\\ concentrazione",false,false,'','','',0],
			["Obbligazioni\\ indicizzate",false,false,'','','',0],
			["Obbligazioni\\ convertibili",false,false,'','','',0],
			["Obbligazioni",false,false,'','','',0],
			["Nuovo\\ Mercato",false,false,'','','',0],
			["No\\ Load",false,false,'','','',0],
			["New\\ Economy",false,false,'','','',0],
			["Negoaziazione\\ continua",false,false,'','','',0],
			["Nasdaq",false,false,'','','',0],
			["Mutuo",false,false,'','','',0],
			["Monte\\ titoli",false,false,'','','',0],
			["Montante",false,false,'','','',0],
			["Money\\ Broker",false,false,'','','',0],
			["Moneta\\ Elettronica",false,false,'','','',0],
			["Mibtel",false,false,'','','',0],
			["Mib",false,false,'','','',0],
			["Mezzi\\ propri",false,false,'','','',0],
			["Metodo\\ di\\ valutazione\\ di\\ un\\ prestito",false,false,'','','',0],
			["Mercato\\ Valutario",false,false,'','','',0],
			["Mercato\\ Ristretto",false,false,'','','',0],
			["Mercato\\ Monetario",false,false,'','','',0],
			["Mercato\\ Grigio",false,false,'','','',0],
			["Mercato\\ Finanziario",false,false,'','','',0],
			["Mercato\\ Borsistico",false,false,'','','',0],
			["Mediatore\\ Creditizio",false,false,'','','',0],
			["Market\\ timing",false,false,'','','',0],
			["Market\\ maker",false,false,'','','',0],
			["Management\\ Fee",false,false,'','','',0],
			["Listino\\ di\\ Borsa",false,false,'','','',0],
			["Liquidità",false,false,'','','',0],
			["Linea\\ di\\ Credito",false,false,'','','',0],
			["Limite\\ legale\\ di\\ fido",false,false,'','','',0],
			["Limite\\ di\\ fido",false,false,'','','',0],
			["Liffe",false,false,'','','',0],
			["Liberatoria",false,false,'','','',0],
			["Leverage\\ \\(o\\ leva\\ finanziaria\\)",false,false,'','','',0],
			["Lettera",false,false,'','','',0],
			["Leasing\\ Operativo",false,false,'','','',0],
			["Leasing\\ Finanziario",false,false,'','','',0],
			["Leasing",false,false,'','','',0],
			["IRS\\ \\(Interest\\ Rate\\ Swap\\)",false,false,'','','',0],
			["Ipoteca",false,false,'','','',0],
			["Ipo",false,false,'','','',0],
			["Investment\\ Bank",false,false,'','','',0],
			["Investimento",false,false,'','','',0],
			["Intermediario\\ Finanziario",false,false,'','','',0],
			["Interessi\\ usurai",false,false,'','','',0],
			["Interessi\\ di\\ Mora",false,false,'','','',0],
			["Interesse",false,false,'','','',0],
			["Insolvenza",false,false,'','','',0],
			["Insider",false,false,'','','',0],
			["Indici\\ borsistici",false,false,'','','',0],
			["Indice\\ total\\ return",false,false,'','','',0],
			["Indice\\ di\\ Sharpe",false,false,'','','',0],
			["IMR",false,false,'','','',0],
			["Idem",false,false,'','','',0],
			["I\\.S\\.C\\.\\ \\(indicatore\\ sintetico\\ di\\ costo\\)",false,false,'','','',0],
			["IBAN",false,false,'','','',0],
			["Hedging",false,false,'','','',0],
			["Hedge\\ Fund",false,false,'','','',0],
			["Global\\ Bond",false,false,'','','',0],
			["Giroconto",false,false,'','','',0],
			["Girata\\ in\\ bianco",false,false,'','','',0],
			["Girata",false,false,'','','',0],
			["Giorno\\ di\\ liquidazione\\ borsistica",false,false,'','','',0],
			["Giorni\\ Valuta",false,false,'','','',0],
			["Giardinetto",false,false,'','','',0],
			["Gestione\\ di\\ cassa",false,false,'','','',0],
			["Gestione\\ del\\ Rischio",false,false,'','','',0],
			["Garanzia",false,false,'','','',0],
			["Garante",false,false,'','','',0],
			["GAP",false,false,'','','',0],
			["Future",false,false,'','','',0],
			["Fondo\\ monetario\\ internazione\\ \\(FMI\\)",false,false,'','','',0],
			["Fondo\\ comune\\ di\\ investimento",false,false,'','','',0],
			["Flusso\\ di\\ cassa",false,false,'','','',0],
			["Floor",false,false,'','','',0],
			["Floating\\ rate\\ note\\ \\(floater\\)",false,false,'','','',0],
			["Finanziamento\\ rateale",false,false,'','','',0],
			["Finanziamento\\ a\\ vista",false,false,'','','',0],
			["Finanziamento\\ a\\ tempo",false,false,'','','',0],
			["Finanziamento\\ a\\ medio\\-lungo\\ termine",false,false,'','','',0],
			["Finanziamento\\ a\\ garanzia\\ limitata",false,false,'','','',0],
			["Fido",false,false,'','','',0],
			["Fidelity\\ Card ",false,false,'','','',0],
			["Fidejussione",false,false,'','','',0],
			["Factoring",false,false,'','','',0],
			["Euromercato",false,false,'','','',0],
			["Eurobbligazioni",false,false,'','','',0],
			["EURO",false,false,'','','',0],
			["EURISC",false,false,'','','',0],
			["EURIRS",false,false,'','','',0],
			["Euribor",false,false,'','','',0],
			["Estinzione\\ anticipata",false,false,'','','',0],
			["Erogazione",false,false,'','','',0],
			["Equalizzatore",false,false,'','','',0],
			["EPS",false,false,'','','',0],
			["EONIA",false,false,'','','',0],
			["ENTERPRISE\\ VALUE",false,false,'','','',0],
			["ECU\\ \\(European\\ Currency\\ Unit\\)",false,false,'','','',0],
			["ECONOMIC\\ VALUE\\ ADDED\\ \\(EVA\\)",false,false,'','','',0],
			["EARNINGS\\ BEFORE\\ INTEREST\\ AND\\ TAXES\\ \\(EBIT\\)",false,false,'','','',0],
			["Duration\\ di\\ portafoglio",false,false,'','','',0],
			["Durata\\ del\\ finanziamento",false,false,'','','',0],
			["Dumping",false,false,'','','',0],
			["Down\\ Jones",false,false,'','','',0],
			["Domanda",false,false,'','','',0],
			["Dividendi",false,false,'','','',0],
			["Disaggio\\ di\\ emissione",false,false,'','','',0],
			["Derivato",false,false,'','','',0],
			["Deposito",false,false,'','','',0],
			["Denaro",false,false,'','','',0],
			["Deficit\\ pubblico",false,false,'','','',0],
			["DEBT\\ RATIO",false,false,'','','',0],
			["Debito\\ residuo",false,false,'','','',0],
			["Debito",false,false,'','','',0],
			["Dealer",false,false,'','','',0],
			["DAX",false,false,'','','',0],
			["Data\\ valuta",false,false,'','','',0],
			["Data\\ di\\ regolamento",false,false,'','','',0],
			["Custodia\\ titoli",false,false,'','','',0],
			["CUD",false,false,'','','',0],
			["CRIF",false,false,'','','',0],
			["Creditore",false,false,'','','',0],
			["Credito\\ finalizzato",false,false,'','','',0],
			["Credito\\ al\\ Consumo",false,false,'','','',0],
			["Credito",false,false,'','','',0],
			["Credit\\ Bureau",false,false,'','','',0],
			["Corso\\ Secco",false,false,'','','',0],
			["Corso",false,false,'','','',0],
			["Convenzione",false,false,'','','',0],
			["Convenzionato\\ \\(Dealer\\)",false,false,'','','',0],
			["Contratto\\ Strip",false,false,'','','',0],
			["Contratto\\ Strap",false,false,'','','',0],
			["Contratto\\ a\\ termine",false,false,'','','',0],
			["Contratto\\ a\\ premio",false,false,'','','',0],
			["Consob",false,false,'','','',0],
			["Commissioni",false,false,'','','',0],
			["Commercio\\ Elettronico",false,false,'','','',0],
			["Circuito\\ di\\ spendibilità",false,false,'','','',0],
			["CIN\\ \\(Control\\ Internal\\ Number\\)",false,false,'','','',0],
			["Cessione\\ del\\ Quinto\\ dello\\ stipendio",false,false,'','','',0],
			["Centrale\\ Rischi",false,false,'','','',0],
			["Cassa\\ di\\ compensazione\\ e\\ garanzia",false,false,'','','',0],
			["Carta\\ di\\ Credito\\ Revolving",false,false,'','','',0],
			["Capitalizzazione\\ di\\ Borsa",false,false,'','','',0],
			["Capitale\\ circolante\\ netto",false,false,'','','',0],
			["Capital\\ Gain",false,false,'','','',0],
			["Caparra",false,false,'','','',0],
			["Cambiale",false,false,'','','',0],
			["Calendario\\ di\\ Borsa",false,false,'','','',0],
			["Cac",false,false,'','','',0],
			["Bureau\\ Score",false,false,'','','',0],
			["BTP\\ Future",false,false,'','','',0],
			["Broker",false,false,'','','',0],
			["Borsino",false,false,'','','',0],
			["Borsa\\ Valori",false,false,'','','',0],
			["Borsa",false,false,'','','',0],
			["Bolla\\ speculativa",false,false,'','','',0],
			["Bilancio",false,false,'','','',0],
			["BIC",false,false,'','','',0],
			["Benchmark",false,false,'','','',0],
			["BBAN",false,false,'','','',0],
			["Baratto",false,false,'','','',0],
			["Banda\\ magnetica",false,false,'','','',0],
			["Bancomat",false,false,'','','',0],
			["Banca\\ Retail",false,false,'','','',0],
			["Banca\\ d’investimento",false,false,'','','',0],
			["Banca\\ dealer",false,false,'','','',0],
			["Banca\\ d’Affari",false,false,'','','',0],
			["Banca\\ Centrale",false,false,'','','',0],
			["Banca",false,false,'','','',0],
			["Back\\-Office",false,false,'','','',0],
			["Azioni\\ privilegiate",false,false,'','','',0],
			["Azione\\ di\\ risparmio",false,false,'','','',0],
			["Azione",false,false,'','','',0],
			["Avversione\\ al\\ rischio",false,false,'','','',0],
			["Attvità\\ finanziaria\\ sottostante",false,false,'','','',0],
			["Attivazione\\ della\\ Carta\\ di\\ Credito",false,false,'','','',0],
			["ATM\\ \\(Automated\\ Teller\\ Machine\\)",false,false,'','','',0],
			["Assogestioni",false,false,'','','',0],
			["Assofin\\ \\(Associazione\\ Italiana\\ del\\ Credito\\ al\\ Consumo\\ e\\ Immobiliare\\)",false,false,'','','',0],
			["Assicurazione\\ Vita",false,false,'','','',0],
			["Assicurazione\\ Mista",false,false,'','','',0],
			["Assicurazione\\ Impiego",false,false,'','','',0],
			["Assicurazione",false,false,'','','',0],
			["Assicuratore",false,false,'','','',0],
			["Assicurato",false,false,'','','',0],
			["Asset\\ Allocation",false,false,'','','',0],
			["Assegno\\ Bancario",false,false,'','','',0],
			["Apertura\\ di\\ Credito\\ Rotativa",false,false,'','','',0],
			["Apertura\\ di\\ Credito",false,false,'','','',0],
			["Anticipo\\ Contante",false,false,'','','',0],
			["Anti\\ trust",false,false,'','','',0],
			["Analisi\\ di\\ bilancio",false,false,'','','',0],
			["Ammortamento",false,false,'','','',0],
			["Agente\\ di\\ cambio",false,false,'','','',0],
			["Affidabilità\\ creditizia",false,false,'','','',0],
			["Advisor\\ –\\ Consulente",false,false,'','','',0],
			["Acquisizione",false,false,'','','',0],
			["Accollo\\ del\\ Mutuo",false,false,'','','',0],
			["ABI\\ \\ \\(associazione\\ Bancaria\\ Italiana\\)",false,false,'','','',0],
						];
			tooltipIds=[
							"28900",
							"28899",
							"28898",
							"28897",
							"28896",
							"28895",
							"28894",
							"28893",
							"28892",
							"28891",
							"28890",
							"28889",
							"28888",
							"28887",
							"28886",
							"28885",
							"28884",
							"28883",
							"28881",
							"28880",
							"28879",
							"28878",
							"28877",
							"28876",
							"28875",
							"28874",
							"28873",
							"28872",
							"28871",
							"28870",
							"28869",
							"28868",
							"28867",
							"28866",
							"28865",
							"28864",
							"28863",
							"28862",
							"28861",
							"28860",
							"28859",
							"28858",
							"28857",
							"28856",
							"28855",
							"28854",
							"28853",
							"28852",
							"28851",
							"28849",
							"28847",
							"28846",
							"28845",
							"28844",
							"28843",
							"28842",
							"28841",
							"28840",
							"28839",
							"28838",
							"28837",
							"28836",
							"28835",
							"28834",
							"28833",
							"28832",
							"28830",
							"28829",
							"28828",
							"28827",
							"28826",
							"28825",
							"28824",
							"28823",
							"28822",
							"28821",
							"28820",
							"28819",
							"28818",
							"28817",
							"28815",
							"28813",
							"28812",
							"28811",
							"28810",
							"28809",
							"28808",
							"28807",
							"28806",
							"28805",
							"28804",
							"28803",
							"28802",
							"28801",
							"28800",
							"28799",
							"28798",
							"28797",
							"28796",
							"28795",
							"28794",
							"28793",
							"28792",
							"28791",
							"28790",
							"28789",
							"28788",
							"28787",
							"28786",
							"28785",
							"28784",
							"28783",
							"28782",
							"28781",
							"28780",
							"28777",
							"28776",
							"28775",
							"28774",
							"28773",
							"28772",
							"28771",
							"28770",
							"28769",
							"28767",
							"28766",
							"28765",
							"28764",
							"28763",
							"28762",
							"28761",
							"28760",
							"28759",
							"28758",
							"28757",
							"28756",
							"28755",
							"28754",
							"28753",
							"28752",
							"28751",
							"28749",
							"28748",
							"28747",
							"28746",
							"28745",
							"28744",
							"28743",
							"28742",
							"28741",
							"28740",
							"28739",
							"28738",
							"28737",
							"28736",
							"28735",
							"28734",
							"28733",
							"28732",
							"28731",
							"28730",
							"28729",
							"28728",
							"28727",
							"28726",
							"28725",
							"28724",
							"28723",
							"28722",
							"28721",
							"28720",
							"28719",
							"28718",
							"28717",
							"28716",
							"28715",
							"28714",
							"28713",
							"28710",
							"28709",
							"28708",
							"28707",
							"28706",
							"28705",
							"28704",
							"28703",
							"28701",
							"28700",
							"28699",
							"28698",
							"28696",
							"28695",
							"28694",
							"28693",
							"28692",
							"28691",
							"28690",
							"28689",
							"28688",
							"28687",
							"28686",
							"28685",
							"28684",
							"28683",
							"28682",
							"28681",
							"28679",
							"28678",
							"28677",
							"28676",
							"28675",
							"28674",
							"28673",
							"28672",
							"28671",
							"28670",
							"28669",
							"28668",
							"28667",
							"28666",
							"28665",
							"28664",
							"28663",
							"28662",
							"28661",
							"28660",
							"28658",
							"28657",
							"28656",
							"28655",
							"28654",
							"28653",
							"28652",
							"28651",
							"28649",
							"28648",
							"28647",
							"28646",
							"28645",
							"28644",
							"28643",
							"28642",
							"28641",
							"28640",
							"28639",
							"28636",
							"28635",
							"28633",
							"28632",
							"28631",
							"28629",
							"28628",
							"28627",
							"28626",
							"28625",
							"28624",
							"28623",
							"28622",
							"28621",
							"28620",
							"28619",
							"28618",
							"28617",
							"28616",
							"28615",
							"28614",
							"28613",
							"28612",
							"28611",
							"28610",
							"28609",
							"28608",
							"28607",
							"28606",
							"28605",
							"28604",
							"28603",
							"28602",
							"28601",
							"28600",
							"28599",
							"28598",
							"28597",
							"28596",
							"28595",
							"28594",
							"28593",
							"28592",
							"28591",
							"28590",
							"28589",
							"28588",
							"28587",
							"28586",
							"28585",
							"28584",
							"28583",
							"28582",
							"28581",
							"28580",
							"28579",
							"28578",
							"28577",
							"28575",
							"28574",
							"28573",
							"28572",
							"28571",
							"28569",
							"28568",
							"28567",
							"28566",
							"28565",
							"28564",
							"28562",
							"28561",
							"28560",
							"28559",
							"28558",
							"28556",
							"28554",
							"28553",
							"28552",
							"28551",
							"28550",
							"28549",
							"28548",
							"28547",
							"28546",
							"28545",
							"28544",
							"28543",
							"28542",
							"28541",
							"28540",
							"28539",
							"28538",
							"28537",
							"28535",
							"28534",
							"28533",
							"28532",
							"28531",
							"28530",
							"28529",
							"28528",
							"28527",
							"28526",
							"28525",
							"28524",
							"28523",
							"28522",
							"28520",
							"28519",
							"28518",
							"28517",
							"28516",
							"28515",
							"28514",
							"28513",
							"28511",
							"28510",
							"28509",
							"28508",
							"28507",
							"28506",
							"28505",
							"28504",
							"28503",
							"28502",
							"28501",
							"28425",
							"28424",
							"28423",
							"28421",
							"28420",
							"28419",
							"28418",
							"28417",
							"28416",
							"28414",
							"28413",
							"28412",
							"28409",
						];

			//include or fetch zone
						var class_to_cover=[
						];
			var tags_to_cover=[
						];
			var areas_to_cover = class_to_cover.concat( tags_to_cover );

			if(areas_to_cover.length==0){//if no classes mentioned
				areas_to_cover.push("body");
			}

			fetch_all="";


			//exclude zone block
			{
				var zones_to_exclude=[
							".kttg_glossary_content", //remove tooltips from inside the glossary content
							"#tooltip_blocks_to_show", //remove tooltips from inside the tooltips
							".td-bred-no-url-last",];
										zones_to_exclude.push("a");
											zones_to_exclude.push("h"+1);
											zones_to_exclude.push("h"+2);
											zones_to_exclude.push("h"+3);
								}

				for(var j=0 ; j<areas_to_cover.length ; j++){
					/*test overlapping classes*/
					var tmp_classes=areas_to_cover.slice(); //affectation par valeur
					//remove current elem from tmp tab
					tmp_classes.splice(j,1);

					//if have parents (to avoid overlapping zones)
						if(
							tmp_classes.length>0
							&&
							jQuery(areas_to_cover[j]).parents(tmp_classes.join(",")).length>0
						){
							continue;
						}
					/*end : test overlapping classes*/


					for(var cls=0 ; cls<jQuery(areas_to_cover[j]).length ; cls++){
						zone=jQuery(areas_to_cover[j])[cls];
						//to prevent errors in unfound classes
						if (zone==undefined) {
							continue;
						}

						for(var i=0;i<kttg_tab.length;i++){

							suffix='';
							if(kttg_tab[i][2]==true){//if is prefix
								suffix='\\w*';
							}
							txt_to_find=kttg_tab[i][0];
							var text_sep='[\\s<>,;:!$^*=\\-()\'"&?.\\/§%£¨+°~#{}\\[\\]|`\\\^@¤]'; //text separator

							//families for class
                            tooltipy_families_class=kttg_tab[i][3];

                            //video class
                            tooltipy_video_class=kttg_tab[i][4];

							/*test japanese and chinese*/
							var japanese_chinese=/[\u3000-\u303F]|[\u3040-\u309F]|[\u30A0-\u30FF]|[\uFF00-\uFFEF]|[\u4E00-\u9FAF]|[\u2605-\u2606]|[\u2190-\u2195]|\u203B/;
						    var jc_reg = new RegExp(japanese_chinese);

							if(jc_reg.test(txt_to_find)){
								//change pattern if japanese or chinese text
								text_sep=""; //no separator for japanese and chinese
							}

							pattern=text_sep+"("+txt_to_find+")"+suffix+""+text_sep+"|^("+txt_to_find+")"+suffix+"$|"+text_sep+"("+txt_to_find+")"+suffix+"$|^("+txt_to_find+")"+suffix+text_sep;

							iscase='';
							if(kttg_tab[i][1]==false){
								iscase='i';
							}
							var reg=new RegExp(pattern,fetch_all+iscase);

							if (typeof findAndReplaceDOMText == 'function') { //if function exists
							  findAndReplaceDOMText(zone, {
									preset: 'prose',
									find: reg,
									replace: function(portion) {

										splitted=portion.text.split(new RegExp(txt_to_find,'i'));
										txt_to_display=portion.text.match(new RegExp(txt_to_find,'i'));
										/*exclude zones_to_exclude*/
										zones_to_exclude_string=zones_to_exclude.join(", ");
										if(
											jQuery(portion.node.parentNode).parents(zones_to_exclude_string).length>0
											||
											jQuery(portion.node.parentNode).is(zones_to_exclude_string)
										){
											return portion.text;
										}
										/*avoid overlaped keywords*/
										if(
											jQuery(portion.node.parentNode).parents(".bluet_tooltip").length>0
											||
											jQuery(portion.node.parentNode).is(".bluet_tooltip")
										){
											return portion.text;
										}
										//number of appearence
																				if(kttg_tab[i][6]==1){
											return portion.text;
										}

										kttg_tab[i][6]++;

										if(splitted[0]!=undefined){ before_kw = splitted[0]; }else{before_kw="";}
										if(splitted[1]!=undefined){ after_kw = splitted[1]; }else{after_kw="";}

										if(portion.text!="" && portion.text!=" " && portion.text!="\t" && portion.text!="\n" ){
											//console.log(i+" : ("+splitted[0]+"-["+txt_to_find+"]-"+splitted[1]+"-"+splitted[2]+"-"+splitted[3]+")");
																								var elem = document.createElement("span");

													if(before_kw==undefined || before_kw==null){
															before_kw="";
													}

													//extract icon if present
													kttg_icon='';

													if(kttg_tab[i][5]!=""){
														kttg_icon='<img src="'+kttg_tab[i][5]+'" >';
													}

													if(suffix!=""){
														var reg=new RegExp(suffix,"");
														suff_after_kw=after_kw.split(reg)[0];

														if(after_kw.split(reg)[0]=="" && after_kw.split(reg)[1]!=undefined){
															suff_after_kw=after_kw.split(reg)[1];
														}

														if(suff_after_kw==undefined){
															suff_after_kw="";
														}

														just_after_kw=after_kw.match(reg);
														if(just_after_kw==undefined || just_after_kw==null){
															just_after_kw="";
														}

														if(suff_after_kw==" "){
                                                            suff_after_kw="  ";
                                                        }

                                                        if(before_kw==" "){
                                                            before_kw="  ";
                                                        }
                                        /*console.log('('+suffix+')('+after_kw.split(reg)[1]+')');
  										console.log('['+after_kw+'] -'+suff_after_kw+'-');*/

  										            //with prefix
														elem.innerHTML=(txt_to_display==undefined || txt_to_display==null) ? before_kw+just_after_kw+suff_after_kw : before_kw+"<span class='bluet_tooltip tooltipy-kw-prefix' data-tooltip="+tooltipIds[i]+">"+kttg_icon+txt_to_display+""+just_after_kw+"</span>"+suff_after_kw;
                                                	}else{
                                                        if(after_kw==" "){
                                                            after_kw="  ";
                                                        }

                                                        if(before_kw==" "){
                                                            before_kw="  ";
                                                        }
                                                        //without prefix
                                                        elem.innerHTML=(txt_to_display==undefined || txt_to_display==null) ? before_kw+after_kw : before_kw+"<span class='bluet_tooltip' data-tooltip="+tooltipIds[i]+">"+kttg_icon+txt_to_display+"</span>"+after_kw;
                                                    }
													//add classes to keywords
                                                    jQuery(jQuery(elem).children(".bluet_tooltip")[0]).addClass("tooltipy-kw tooltipy-kw-"+tooltipIds[i]+" "+tooltipy_families_class+" "+tooltipy_video_class+" ");

													return elem;


										}else{
												return "";
										}
									}
								});
							}

						}
					}
				}
			//trigger event sying that keywords are fetched
			jQuery.event.trigger("keywordsFetched");
		}
			/*end test*/

			jQuery(document).ready(function(){
				tltpy_fetch_kws();

				bluet_placeTooltips(".bluet_tooltip, .bluet_img_tooltip","bottom",true);
				animation_type="none";
				animation_speed="";
				moveTooltipElementsTop(".bluet_block_to_show");
			});

			jQuery(document).on("keywordsLoaded",function(){
				bluet_placeTooltips(".bluet_tooltip, .bluet_img_tooltip","bottom",false);
			});

			/*	Lanch keywords fetching for a chosen event triggered - pro feature	*/

		</script>
							<script>
				jQuery(document).ready(function(){
						/*test begin*/
					load_tooltip="<span id='loading_tooltip' class='bluet_block_to_show' data-tooltip='0'>";
						load_tooltip+="<div class='bluet_block_container'>";
							load_tooltip+="<div class='bluet_text_content'>";
									load_tooltip+="<img width='15px' src='https://www.prestitotto.it/wp-content/plugins/wp-tooltipy/assets/loading.gif' />";
							load_tooltip+="</div>";
						load_tooltip+="</div>";
					load_tooltip+="</span>";

					jQuery("#tooltip_blocks_to_show").append(load_tooltip);
					/*test end*/
				});
			</script>


    <!--

        Theme: Newspaper by tagDiv.com 2017
        Version: 9.5 (rara)
        Deploy mode: deploy

        uid: 5ec7a673d303a
    -->

    <script type='text/javascript' src='https://www.prestitotto.it/wp-content/plugins/strumenti_calcolo/assets/js/jquery.blockUI.js?ver=2.70.0'></script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-content/plugins/strumenti_calcolo/assets/js/strumenti.js?ver=1.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ctcc_vars = {"expiry":"30","method":"","version":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-content/plugins/uk-cookie-consent/assets/js/uk-cookie-consent-js.js?ver=2.3.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var tltpy_ajax_load = "https:\/\/www.prestitotto.it\/wp-admin\/admin-ajax.php";
/* ]]> */
</script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-content/plugins/wp-tooltipy/advanced/assets/ajax/load-keywords.js?ver=5.1.3'></script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-content/plugins/wp-tooltipy/assets/kttg-tooltip-functions.js?ver=5.1.3'></script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=4.9.14'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7_redirect_forms = {"34239":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie-candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"33874":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"33851":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie-candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"33042":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"33041":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"32853":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie_candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"32791":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"32532":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie_candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"31529":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie_candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"31505":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie_candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"31487":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie_candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"31104":{"page_id":"19","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"https:\/\/www.prestitotto.it\/"},"31061":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie-candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"31048":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie_candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"30617":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie_candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"30612":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie_candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"30606":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie_candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"30604":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"30558":{"page_id":"0","external_url":"https:\/\/www.prestitotto.it\/grazie_candidatura\/","use_external_url":"on","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"30552":{"page_id":"19","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"https:\/\/www.prestitotto.it\/"},"29856":{"page_id":"","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"29241":{"page_id":"32","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":false},"29137":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"28336":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"28295":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-content/plugins/wpcf7-redirect/js/wpcf7-redirect-script.js'></script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-content/themes/Newspaper/js/tagdiv_theme.min.js?ver=9.5'></script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-includes/js/wp-embed.min.js?ver=4.9.14'></script>
<script type='text/javascript' src='https://www.prestitotto.it/wp-content/plugins/wp-tooltipy/advanced/assets/kttg-pro-functions.js?ver=5.1.3'></script>

<!-- JS generated by theme -->

<script>

jQuery().ready(function() {
var pulldown_size = jQuery(".td-category-pulldown-filter:first").width();
if (pulldown_size > 113) { jQuery(".td-category-pulldown-filter .td-pulldown-filter-list").css({"min-width": pulldown_size, "border-top": "1px solid #444"}); }
});



		(function(){
			var html_jquery_obj = jQuery('html');

			if (html_jquery_obj.length && (html_jquery_obj.is('.ie8') || html_jquery_obj.is('.ie9'))) {

				var path = 'https://www.prestitotto.it/wp-content/themes/Newspaper/style.css';

				jQuery.get(path, function(data) {

					var str_split_separator = '#td_css_split_separator';
					var arr_splits = data.split(str_split_separator);
					var arr_length = arr_splits.length;

					if (arr_length > 1) {

						var dir_path = 'https://www.prestitotto.it/wp-content/themes/Newspaper';
						var splited_css = '';

						for (var i = 0; i < arr_length; i++) {
							if (i > 0) {
								arr_splits[i] = str_split_separator + ' ' + arr_splits[i];
							}
							//jQuery('head').append('<style>' + arr_splits[i] + '</style>');

							var formated_str = arr_splits[i].replace(/\surl\(\'(?!data\:)/gi, function regex_function(str) {
								return ' url(\'' + dir_path + '/' + str.replace(/url\(\'/gi, '').replace(/^\s+|\s+$/gm,'');
							});

							splited_css += "<style>" + formated_str + "</style>";
						}

						var td_theme_css = jQuery('link#td-theme-css');

						if (td_theme_css.length) {
							td_theme_css.after(splited_css);
						}
					}
				});
			}
		})();



</script>


				<script type="text/javascript">
					jQuery(document).ready(function($){
												if(!catapultReadCookie("catAccCookies")){ // If the cookie has not been set then show the bar
							$("html").addClass("has-cookie-bar");
							$("html").addClass("cookie-bar-bottom-bar");
							$("html").addClass("cookie-bar-bar");
													}
																	});
				</script>

			<div id="catapult-cookie-bar" class=""><div class="ctcc-inner "><span class="ctcc-left-side">Questo sito utilizza cookie di profilazione, propri oppure di terze parti per rendere migliore l&#039;esperienza d&#039;uso degli utenti. Chiudendo questo banner, scorrendo questa pagina oppure cliccando qualunque suo elemento acconsenti all’uso dei cookie.      Per maggiori informazioni <a class="ctcc-more-info-link" tabindex=0 target="_blank" href="https://www.prestitotto.it/cookie-policy/">clicca qui</a></span><span class="ctcc-right-side"><button id="catapultCookie" tabindex=0 onclick="catapultAcceptCookies();">Accetto</button></span></div><!-- custom wrapper class --></div><!-- #catapult-cookie-bar -->

			<div id="tdw-css-writer" style="display: none" class="tdw-drag-dialog tdc-window-sidebar">
				<header>


					<a title="Editor" class="tdw-tab tdc-tab-active" href="#" data-tab-content="tdw-tab-editor">Edit with Live CSS</a>
					<div class="tdw-less-info" title="This will be red when errors are detected in your CSS and LESS"></div>

				</header>
				<div class="tdw-content">


					<div class="tdw-tabs-content tdw-tab-editor tdc-tab-content-active">


						<script>

							(function(jQuery, undefined) {

								jQuery(window).ready(function() {

									if ( 'undefined' !== typeof tdcAdminIFrameUI ) {
										var $liveIframe  = tdcAdminIFrameUI.getLiveIframe();

										if ( $liveIframe.length ) {
											$liveIframe.load(function() {
												$liveIframe.contents().find( 'body').append( '<textarea class="tdw-css-writer-editor" style="display: none"></textarea>' );
											});
										}
									}

								});

							})(jQuery);

						</script>


						<textarea class="tdw-css-writer-editor td_live_css_uid_1_5ec7a673d365f"></textarea>
						<div id="td_live_css_uid_1_5ec7a673d365f" class="td-code-editor"></div>


						<script>
							jQuery(window).load(function (){

								if ( 'undefined' !== typeof tdLiveCssInject ) {

									tdLiveCssInject.init();


									var editor_textarea = jQuery('.td_live_css_uid_1_5ec7a673d365f');
									var languageTools = ace.require("ace/ext/language_tools");
									var tdcCompleter = {
										getCompletions: function (editor, session, pos, prefix, callback) {
											if (prefix.length === 0) {
												callback(null, []);
												return
											}

											if ('undefined' !== typeof tdcAdminIFrameUI) {

												var data = {
													error: undefined,
													getShortcode: ''
												};

												tdcIFrameData.getShortcodeFromData(data);

												if (!_.isUndefined(data.error)) {
													tdcDebug.log(data.error);
												}

												if (!_.isUndefined(data.getShortcode)) {

													var regex = /el_class=\"([A-Za-z0-9_-]*\s*)+\"/g,
														results = data.getShortcode.match(regex);

													var elClasses = {};

													for (var i = 0; i < results.length; i++) {
														var currentClasses = results[i]
															.replace('el_class="', '')
															.replace('"', '')
															.split(' ');

														for (var j = 0; j < currentClasses.length; j++) {
															if (_.isUndefined(elClasses[currentClasses[j]])) {
																elClasses[currentClasses[j]] = '';
															}
														}
													}

													var arrElClasses = [];

													for (var prop in elClasses) {
														arrElClasses.push(prop);
													}

													callback(null, arrElClasses.map(function (item) {
														return {
															name: item,
															value: item,
															meta: 'in_page'
														}
													}));
												}
											}
										}
									};
									languageTools.addCompleter(tdcCompleter);

									window.editor = ace.edit("td_live_css_uid_1_5ec7a673d365f");

									// 'change' handler is written as function because it's called by tdc_on_add_css_live_components (of wp_footer hook)
									// We did it to reattach the existing compiled css to the new content received from server.
									window.editorChangeHandler = function () {
										//tdwState.lessWasEdited = true;

										window.onbeforeunload = function () {
											if (tdwState.lessWasEdited) {
												return "You have attempted to leave this page. Are you sure?";
											}
											return false;
										};

										var editorValue = editor.getSession().getValue();

										editor_textarea.val(editorValue);

										if ('undefined' !== typeof tdcAdminIFrameUI) {
											tdcAdminIFrameUI.getLiveIframe().contents().find('.tdw-css-writer-editor:first').val(editorValue);

											// Mark the content as modified
											// This is important for showing info when composer closes
                                            tdcMain.setContentModified();
										}

										tdLiveCssInject.less();
									};

									editor.getSession().setValue(editor_textarea.val());
									editor.getSession().on('change', editorChangeHandler);

									editor.setTheme("ace/theme/textmate");
									editor.setShowPrintMargin(false);
									editor.getSession().setMode("ace/mode/less");
									editor.setOptions({
										enableBasicAutocompletion: true,
										enableSnippets: true,
										enableLiveAutocompletion: false
									});

								}

							});
						</script>

					</div>
				</div>

				<footer>


						<a href="#" class="tdw-save-css">Save</a>
						<div class="tdw-more-info-text">Write CSS OR LESS and hit save. CTRL + SPACE for auto-complete.</div>


					<div class="tdw-resize"></div>
				</footer>
			</div>

</body>
</html>

<script>
// Get the modal
var modal = document.getElementById("myModal");

function openModal(id){
  // Get the button that opens the modal
  var btn = document.getElementById(id);

  var codice = document.getElementById(id).getAttribute("data-codice");
  document.getElementById("codice").value = codice;

  var url = document.getElementById(id).getAttribute("data-url");
  document.getElementById("url").href = url;

  var nome = document.getElementById(id).getAttribute("data-nome");
  document.getElementById("codicenome").innerHTML = nome;

  modal.style.display = "block";
};

function closeModal(){
  document.getElementById("codice").value = '';
  document.getElementById("url").href = '';
  document.getElementById("codicenome").innerHTML = '';
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<style>
.codici-sconto-description {
  float: left;
  width: 100%;
  color: #333;
  font-size: 14px;
  line-height: 18px;
  text-align: justify;
}

.text-holder {
  font-size: 14px;
  text-align: center;
  line-height: 18px;
  margin-bottom: 15px;
}

.prestitotto-categories-top20-container {
  display: block;
  width: 100%;
  float: left;
  transition: opacity 0.2s linear;
  margin-top: 20px;
}
.prestitotto-categories-top20-container .best-offers {
  display: block;
  float: left;
  width: 100%;
}
.prestitotto-categories-top20-container .best-offers .best-offers-holder {
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  border-bottom: 1px solid #d6dadc;
  height: 37px;
  line-height: normal;
}
.prestitotto-categories-top20-container .best-offers .top20-t {
  width: auto;
  float: left;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  display: flex;
  align-items: flex-start;
}
.prestitotto-categories-top20-container .best-offers .categories-item-title {
  width: 54%;
  color: #333;
  font-size: 20px;
  font-weight: bold;
  padding: 0 10px 0 0;
  float: left;
}
.prestitotto-categories-top20-container .best-offers .top20-t .categories-item-title {
  width: auto;
  display: inline;
  float: none;
}

.prestitotto-categories-top20-container .best-offers .categories-item-title {
  width: 54%;
  color: #333;
  font-size: 20px;
  font-weight: bold;
  padding: 0 10px 0 0;
  float: left;
}

.prestitotto-categories-top20-container .best-offers .top20-t .categories-item-title {
  width: auto;
  display: inline;
  float: none;
}

.prestitotto-categories-top20-container .best-offers .top20-t span.categories-item-title {
  overflow: hidden;
  text-overflow: ellipsis;
}

.hidden {
  display: none;
}

.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 15% auto;
  padding: 20px;
  border: 1px solid #888;
  width: 312px;
  text-align: center;
}

#codice-container{
  margin: 20px;
}

#codice{
  width: auto;
  text-align: center;
}

/* The Close Button */
.close {
  color: #aaa;
  font-size: 28px;
  font-weight: bold;
  text-align: right;
}

.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}

.card{
  width: 85%;
  margin: auto;
  text-align: center;
  display: block;
  min-height: 260px;
}

.card-body {
  position: absolute;
  bottom: 0;
  width: 100%;
}

.card-title{
  font-size: 14px;
  text-align: center;
}

.btn{
  padding: 5px;
}

.button-center{
  text-align: center;
}

.card-img-top{
  padding: 15px;
  width: 200px;
}



.pagination>.active>a, .pagination>.active>span, .pagination>.active>a:hover, .pagination>.active>span:hover, .pagination>.active>a:focus, .pagination>.active>span:focus {
  z-index: 2;
  color: #fff;
  cursor: default;
  background-color: #428bca;
  border-color: #428bca;
}
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
  z-index: 0;
  color: #fff;
  cursor: default;
  background-color: #337ab7;
  border-color: #337ab7;
}
.pagination-sm>li>a, .pagination-sm>li>span {
  padding: 5px 10px;
  font-size: 12px;
}
.pagination>li>a, .pagination>li>span {
  position: relative;
  float: left;
  padding: 6px 12px;
  margin-left: -1px;
  line-height: 1.42857143;
  color: #428bca;
  text-decoration: none;
  background-color: #fff;
  border: 1px solid #ddd;
}
.pagination-sm>li>a, .pagination-sm>li>span {
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
}
.pagination>li>a, .pagination>li>span {
  position: relative;
  float: left;
  padding: 6px 12px;
  margin-left: -1px;
  line-height: 1.42857143;
  color: #337ab7;
  text-decoration: none;
  background-color: #fff;
  border: 1px solid #ddd;
}

</style>
