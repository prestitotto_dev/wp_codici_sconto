<?php
/**
* Plugin Name: Craweler per i Codici Sconto
* Plugin URI: https://www.prestitotto.it/
* Description: Crawler di codici sconto da 2 siti e visualizzazione
* Version: 1.0
* Author: Biagio Grimaldi, Francesco Di Cristo
**/

define( 'PRESTITOTTO_PATH', plugin_dir_path( __FILE__ ) );
include_once( PRESTITOTTO_PATH . 'core/config.php');

/*
 * Initializes the plugin by setting filters and administration functions.
 */

include_once( PRESTITOTTO_PATH . 'page_templater.php');
include_once( PRESTITOTTO_PATH . 'functions.php');

?>
