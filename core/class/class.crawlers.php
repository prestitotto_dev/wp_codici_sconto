<?
  include_once(PRESTITOTTO_PATH."core/config.php");

  class Crawler {
		function __construct(){ }

    public function file_get_contents_proxy($url){
      $proxy = '78.24.101.86:8080';
      $context_array = array('https'=>array('proxy'=>$proxy,'request_fulluri'=>true));
      $context = stream_context_create($context_array);
      $data = file_get_contents($url,false,$context);
      return $data;
    }

    public function _codici_sconto_ansa(){
      global $cfg; global $wpdb;
      $format = array(
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%d',
      );
      $res = array();

      $uri = "https://www.ansa.it/codici-sconto/";
      $html_page = self::file_get_contents_proxy($uri.'sconti-negozi.html');
      $saw_page = new nokogiri($html_page);
      $link_negozi = $saw_page->get('ul.filtriAlfaExpanded li a')->toArray();

      $timestamp = time();
      $delete_codici = "DELETE FROM `np_codici_sconto` WHERE `timestamp` < $timestamp".
      " AND `link` LIKE '%ansa.it%';";
      $wpdb->query($delete_codici);

      foreach($link_negozi as $key => $link_negozio){
        $shop_url = $link_negozio['href'];
        $html_shop_page = self::file_get_contents_proxy($shop_url);
        $saw_shop_page = new nokogiri($html_shop_page);
        $link_codici = $saw_shop_page->get('a#discoverCode')->toArray();
        $logo_url = $saw_shop_page->get('img.logonegozio')->toArray()[0]['src'];

        foreach($link_codici as $key => $link_codice){
          $cid = explode('?cid=', $link_codice['href'])[1];
          if(in_array($cid, $res)){ continue; }
          $res[] = $cid;
          $load_url = $uri.'load_modals.aspx?cid='.$cid;
          $codice_html = self::file_get_contents_proxy($load_url);
          $codice_page = new nokogiri($codice_html);
          $codice_name = $codice_page->get('h2')->toArray()[0]['#text'][0];
          $clear_name = utf8_decode($codice_name);
          $codice_containers = $codice_page->get('div.codiceSconto')->toArray();
          if(count($codice_containers) > 1){
            $codice_codice = $codice_containers[1]['a'][0]['data-clipboard-text'];
            $codice_url = $uri.$codice_containers[1]['a'][0]['href'];
            $codice = array(
              'nome' => $clear_name,
              'codice' => $codice_codice,
              'link' => $codice_url,
              'logo' => $logo_url,
              'testo' => '',
              'timestamp' => $timestamp,
            );
            $wpdb->insert('np_codici_sconto', $codice, $format);
          }
        }
      }
    }


    public function _codici_sconto_discoup(){
      global $cfg; global $wpdb;
      $format = array(
        '%s',
        '%s',
        '%s',
        '%s',
        '%d',
      );
      $res = array();

      $uri = "https://www.discoup.com/";
      $html_uri = self::file_get_contents_proxy('https://www.discoup.com/migliori-codici-sconto.html');
      $saw = new nokogiri($html_uri);
      $links = $saw->get('div.box-offerta a')->toArray();

      $timestamp = time();
      $delete_codici = "DELETE FROM `np_codici_sconto` WHERE `timestamp` < $timestamp".
      " AND `link` LIKE '%discoup.com%';";
      $wpdb->query($delete_codici);

      foreach($links as $key => $link){
        $url = "https://www.discoup.com/".$link['href'];
        $html_url = self::file_get_contents_proxy($url);
        $saw_offer = new nokogiri($html_url);
        $codici_sconto = $saw_offer->get('a.btn-scopri')->toArray();
        $logo = $saw_offer->get('div.sidebar figure.border img')->toArray();
        if(count($logo)>0){ $logo = $logo[0]['src']; } else { $logo = ''; }

        foreach($codici_sconto as $key => $sconto){
          $url_page = "https://www.discoup.com/".str_replace('../', '', $sconto['href']);
          $html_url2 = self::file_get_contents_proxy($url_page);
          $saw_data = new nokogiri($html_url2);
          $data_url = $saw_data->get('a.btn-pink')->toArray();
          $data_url = str_replace('../', '', $data_url[0]['href']);
    			$data_url = $uri.$data_url;
          $data_title = $saw_data->get('h1')->toArray();
          $data_title = $data_title[0]['#text'][0];
          $clear_name = utf8_decode($data_title);
          $data_codice = $saw_data->get('div#codiceTest')->toArray();
          if(count($data_codice) > 0){
            $data_codice = $data_codice[0]['#text'][0];
          } else {
            $data_codice = $saw_data->get('span#codiceTest')->toArray();
            if(count($data_codice)>0){
              $data_codice = $data_codice[0]['#text'][0];
            }
          }

          if($data_codice){
            $codice = array(
              'nome' => $clear_name,
              'codice' => $data_codice,
              'link' => $data_url,
              'logo' => $logo,
              'timestamp' => $timestamp,
            );
            $wpdb->insert('np_codici_sconto', $codice, $format);
          }

        }
      }
    }
    return 'ok';
	}
?>
