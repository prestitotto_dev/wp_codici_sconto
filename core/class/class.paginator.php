<?php

class Paginator {

  private $_limit;
  private $_page;
  private $_total;

  public function __construct( $query_string, $page, $limit, $total ) {
    $this->_query_string   = $query_string;
    $this->_limit   = $limit;
    $this->_page    = $page;
    $this->_total   = $total;
  }

  public function createLinks( $links, $list_class ) {
      if ( $this->_limit == 'all' ) {
          return '';
      }

      $last       = ceil( $this->_total / $this->_limit );

      $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
      $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;

      $html       = '<ul class="' . $list_class . '">';

      if (empty($this->_query_string)){
        $qs = '?';
      } else {
        $qs = "?".$this->_query_string."&";
      }

      $class      = ( $this->_page == 1 ) ? "disabled" : "";
      $html       .= '<li class="' . $class . '"><a href="'.$qs.'x=' . ( $this->_page - 1 ) . '">&laquo;</a></li>';

      if ( $start > 1 ) {
          $html   .= '<li><a href="'.$qs.'x=1">1</a></li>';
          $html   .= '<li class="disabled"><span>...</span></li>';
      }

      for ( $i = $start ; $i <= $end; $i++ ) {
          $class  = ( $this->_page == $i ) ? "active" : "";
          $html   .= '<li class="' . $class . '"><a href="'.$qs.'x=' . $i . '">' . $i . '</a></li>';
      }

      if ( $end < $last ) {
          $html   .= '<li class="disabled"><span>...</span></li>';
          $html   .= '<li><a href="'.$qs.'x=' . $last . '">' . $last . '</a></li>';
      }

      $class      = ( $this->_page == $last ) ? "disabled" : "";
      $html       .= '<li class="' . $class . '"><a href="'.$qs.'x=' . ( $this->_page + 1 ) . '">&raquo;</a></li>';

      $html       .= '</ul>';

      return $html;
  }

}
