<?php

  class CodiciSconto {
    function __construct(){
  		$this->table = 'np_codici_sconto';
  	}

    public function all($limit=20, $offset=0) {
      global $wpdb;
      $sql = "SELECT * FROM `$this->table` WHERE 1".
      " ORDER BY RAND()".
      " LIMIT $limit OFFSET $offset;";
      return $wpdb->get_results($sql);
    }

    public function count() {
      global $wpdb;
      $sql = "SELECT COUNT(*) FROM `$this->table` WHERE 1";
      return $wpdb->get_var($sql);
    }
  }

?>
